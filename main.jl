# Author: Lucas Zenichi Terada
# Institution: University of Campinas

__precompile__()

using Interact
using Blink
using CSV
using TableView
using DataFrames
using LinearAlgebra
using IterativeSolvers
using JSON
using PlotlyJS

#powerflow
include("src/powerflow/powerflow.jl")
include("src/powerflow/analysis.jl")
include("gui/widgets.jl")
include("gui/functions.jl")
include("src/powerflow/fastdeclouped.jl")
include("src/powerflow/declouped.jl")
include("src/powerflow/alternate.jl")
include("src/powerflow/switches.jl")
include("src/powerflow/linearized.jl")
include("gui/powerflow.jl")
include("gui/radialpowerflow.jl")
#state estimation
include("gui/stateestimation.jl")
include("src/estimation/normalequation.jl")
include("src/estimation/functions.jl")
include("src/estimation/varfunctions.jl")
include("src/estimation/sparsetableau.jl")
include("src/estimation/estimationresult.jl")
#Short circuit
include("gui/shortcircuit.jl")
include("src/shortcircuit/threephase.jl")
include("src/shortcircuit/biphasic.jl")
include("src/shortcircuit/impedance.jl")
include("src/shortcircuit/PSimpedance.jl")
include("src/shortcircuit/PSthreephase.jl")
include("src/shortcircuit/PSbiphasic.jl")
include("src/shortcircuit/PSsinglephase.jl")
include("src/shortcircuit/PSshortcircuit.jl")
#Restoration
include("gui/restoration.jl")
include("src/restoration/essentials.jl")
include("src/restoration/powerflow.jl")
include("src/restoration/optimization.jl")
include("src/restoration/tabusearch.jl")
#Reconfiguration
include("gui/reconfiguration.jl")
include("src/reconfiguration/optimization.jl")

#Define global variables
Ωb = DataFrame()
Ωl = DataFrame()
Ωz = DataFrame()
Ωx = DataFrame()
Ωsw = DataFrame()
Ωtr = DataFrame()
Ωgen = DataFrame()
Ωdata = DataFrame()
#Observable variables
Ωbars = Observable{Any}(DataFrame);
Ωbranches = Observable{Any}(DataFrame);
Ωparameters = Observable{Any}(DataFrame);
Ωswitches = Observable{Any}(DataFrame);
Ωmeasures = Observable{Any}(DataFrame);
Ωvariables = Observable{Any}(DataFrame);
Ωtransformers = Observable{Any}(DataFrame);
Type = "None";

#Create the GUI Window
w = Blink.Window()
defaultpage()

on(n -> powerflowpage(),PowerflowButton)
on(n -> radialpage(), RadialButton)
on(n -> estimationpage(),EstimationButton)
on(n -> shortcircuitpage(),ShortcircuitButton)
on(n-> restorationpage(), RestorationButton)
on(n-> reconfigurationpage(), ReconfigurationButton)
on(n -> defaultpage(),BackButton)
on(n -> select(),StartButton)


map!(CSV.read, Ωbars, busbutton)
map!(CSV.read, Ωbranches, networkbutton)
map!(CSV.read, Ωswitches, switchbutton)
map!(CSV.read, Ωparameters, databutton)
map!(CSV.read, Ωmeasures, measuresbutton)
map!(CSV.read, Ωvariables, statebutton)
map!(CSV.read, Ωtransformers, trafobutton)

function powerflowpage()
    global Type = "Power Flow"
    ui = vbox(
        hbox(pad(1em,description)),
        pad(1em,Interact.latex("\\text{Please insert the data files}")),
        pad(1em,busbutton),
        pad(1em,networkbutton),
        pad(1em,databutton),
        pad(1em,switchbox),
        pad(1em,switchbutton),
        pad(1em,powerflowtype),
        hbox(pad(2em,BackButton),pad(2em,StartButton))
    )
    body!(w,ui);
end

function executepowerflow()
    global Ωb = DataFrame(Ωbars.val)
    global Ωl = DataFrame(Ωbranches.val)
    global Ωdata = DataFrame(Ωparameters.val)
    if switchbox[] == true
        global Ωsw = DataFrame(Ωswitches.val);
        global Ωsw.pos = Ωsw.ini;
    end
    
    time = @elapsed begin
        (Y,Κ) = admittance();
        if switchbox[] == true
            (Ysw,Κsw) = switches()
            Y = Y+Ysw;
            K = append!.(Κ,Κsw)
        end
        if powerflowtype[] == "Newton-Raphson"
            (V,θ,Pcalc,Qcalc,ϵ,iterations) = powerflow(Y,Κ);
        elseif powerflowtype[] == "Declouped"
            (V,θ,Pcalc,Qcalc,ϵ,iterations) = declouped(Y,Κ);
        elseif powerflowtype[] == "Alternate Declouped"
            (V,θ,Pcalc,Qcalc,ϵ,iterations) = alternate(Y,Κ);
        elseif powerflowtype[] == "Fast Declouped"
            (V,θ,Pcalc,Qcalc,ϵ,iterations) = fastdeclouped(Y,Κ);
        elseif powerflowtype[] == "Linearized"
            size = length(eachrow(Ωb));
            (θ,P) = linearized()
            V = ones(Float64,size);
            Pcalc = P/Ωdata.Snom[1];
            Qcalc = ones(Float64,size)*NaN;
            ϵ = NaN;
            iterations = NaN;
        end
    end
    if switchbox[] == true
        (Ωbus,Ωline,Ωstatus) = analysis(V,θ,Pcalc,Qcalc,ϵ,iterations,time)
        Ωkeys = keyanalysis(V,θ)
        ui = vbox(
            hbox(pad(1em,description)),
            hbox(pad(1em,Interact.latex("\\text{Results of problem}")),pad(1em,BackButton)),
            pad(1em,Interact.latex("\\mathbf{\\text{Network status:}}")),
            pad(1em,TableView.showtable(Ωstatus)),
            pad(1em,Interact.latex("\\mathbf{\\text{Bus status:}}")),
            pad(1em,TableView.showtable(Ωbus)),
            pad(1em,Interact.latex("\\mathbf{\\text{Lines status:}}")),
            pad(1em,TableView.showtable(Ωline)),
            pad(1em,Interact.latex("\\mathbf{\\text{Switches status:}}")),
            pad(1em,TableView.showtable(Ωkeys))
        )
    else
        (Ωbus,Ωline,Ωstatus) = analysis(V,θ,Pcalc,Qcalc,ϵ,iterations,time)
        ui = vbox(
            hbox(pad(1em,description)),
            hbox(pad(1em,Interact.latex("\\text{Results of problem}")),pad(1em,BackButton)),
            pad(1em,Interact.latex("\\mathbf{\\text{Network status:}}")),
            pad(1em,TableView.showtable(Ωstatus)),
            pad(1em,Interact.latex("\\mathbf{\\text{Bus status:}}")),
            pad(1em,TableView.showtable(Ωbus)),
            pad(1em,Interact.latex("\\mathbf{\\text{Lines status:}}")),
            pad(1em,TableView.showtable(Ωline))
        )
    end
    body!(w,ui);
end
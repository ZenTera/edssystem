#Author: Lucas Zenichi Terada
#Institution: University of Campinas

#logoimage =HTML()
description     = Interact.Widgets.latex("{\\Large\\mathbf{\\text{EDDSystem}}}- \\large{
    \\text{Tools for Power Distribution Problems Based on Julia}}")
PowerflowButton       = Interact.button("Power Flow",style=Dict(:color => "white",
    :backgroundColor => "light green",:width => "250px",:height => "70px",:fontSize => "2em"))
EstimationButton      = Interact.button("State Estimation",style=Dict(:color => "white",
    :backgroundColor => "light green",:width => "250px",:height => "70px",:fontSize => "2em"))
ShortcircuitButton      = Interact.button("Short Circuit",style=Dict(:color => "white",
    :backgroundColor => "light green",:width => "250px",:height => "70px",:fontSize => "2em"))    
RestorationButton     = Interact.button("Restoration",style=Dict(:color => "white",
    :backgroundColor => "light green",:width => "250px",:height => "70px",:fontSize => "2em"))
ReconfigurationButton = Interact.button("Reconfiguration",style=Dict(:color => "white",
    :backgroundColor => "light green",:width => "250px",:height => "70px",:fontSize => "2em"))
RadialButton       = Interact.button("Radial Networks",style=Dict(:color => "white",
    :backgroundColor => "light green",:width => "250px",:height => "70px",:fontSize => "2em"))

CancelButton = Interact.button("Cancel",style=Dict(:color => "white",
    :backgroundColor => "red",:width => "250px",:height => "70px",:fontSize => "2em"))
BackButton   = Interact.button("Back",style=Dict(:color => "white",
    :backgroundColor => "blue",:width => "300px",:height => "50px",:fontSize => "1em"))
StartButton  = Interact.button("Start",style=Dict(:color => "white",
    :backgroundColor => "light green",:width => "300px",:height => "50px",:fontSize => "1em"))

busbutton       = Interact.filepicker(label="\u2002\u2003 Bus Data \u2003\u2002", multiple=false,accept=".csv")
networkbutton   = Interact.filepicker(label="\u2000Branches Data\u2000", multiple=false,accept=".csv")
switchbutton    = Interact.filepicker(label="\u2000Switches Data\u2000", multiple=false,accept=".csv")
databutton      = Interact.filepicker(label="Parameters Data ", multiple=false,accept=".csv")
parbutton       = Interact.filepicker(label="Parameters Data ", multiple=false,accept=".jl")
switchbox       = Interact.checkbox(label="choose if the problem has a switches file");
measuresbutton  = Interact.filepicker(label="\u2000Measures Data\u2000", multiple=false,accept=".csv")
statebutton     = Interact.filepicker(label="\u2002\u2003State Data\u2002\u2003", multiple=false,accept=".csv")
trafobox        = Interact.checkbox(label="choose if the problem has a transformers file");
trafobutton     = Interact.filepicker(label="Transformers Data", multiple=false,accept=".csv")

options = Observable(["Choose the type of method","Newton-Raphson", 
"Declouped", "Alternate Declouped", "Fast Declouped", "Linearized"]);
powerflowtype = dropdown(options);
options = Observable(["Choose the type of method", "Normal Equation", "Sparse Tableau"]);
estimationtype = dropdown(options);
options = Observable(["Choose the type of method", "Three-phase", "Three-phase to ground", "Biphasic", "Biphasic to ground","Single-phase"]);
shortcircuittype = dropdown(options);
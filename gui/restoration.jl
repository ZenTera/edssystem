function restorationpage()
    global Type = "Restoration"
    ui = vbox(
        hbox(pad(1em,description)),
        pad(1em,Interact.latex("\\text{Please insert the data files}")),
        pad(1em,busbutton),
        pad(1em,networkbutton),
        pad(1em,switchbutton),
        pad(1em,parbutton),
        hbox(pad(2em,BackButton),pad(2em,StartButton))
    )
    body!(w,ui);
end

function startrestoration()
    global Ωb = DataFrame(Ωbars.val);
    global Ωl = DataFrame(Ωbranches.val);
    global Ωsw = DataFrame(Ωswitches.val);
    global Ωsw.pos = Ωsw.ini;
    Ωl.r = Ωl.r/1000;
    Ωl.x = Ωl.x/1000;
    Ωsw.r = Ωsw.r/1000;
    Ωsw.x = Ωsw.x/1000;
    include(parbutton[])
    time = @elapsed begin
        (iter, best_cost, list_size) = tabusearch(BTmax, Vmax, Vmin)
    end
    print("TEMPO DE EXECUÇÃO: ", time)
    (Ωstatus, Ωbus, Ωline, Ωkeys) = formatter(iter, best_cost, list_size, time)
    trace1 = scatter(x=1:length(eachrow(Ωb)), y=Ωb.Vl)
    # trace2 = scatter(x=1:length(eachrow(Ωb)), y=Ωb.baseKV*0.93)
    # trace3 = scatter(x=1:length(eachrow(Ωb)), y=Ωb.baseKV*1.05)
    # p = plot([trace1,trace2,trace3])
    p = plot(trace1)
    ui = vbox(
        hbox(pad(1em,description)),
        hbox(pad(1em,Interact.latex("\\text{Results of problem}")),pad(1em,BackButton)),
        pad(1em,Interact.latex("\\mathbf{\\text{Network status:}}")),
        pad(1em,TableView.showtable(Ωstatus)),
        pad(1em,Interact.latex("\\mathbf{\\text{Voltage profile:}}")),
        p,
        pad(1em,Interact.latex("\\mathbf{\\text{Bus status:}}")),
        pad(1em,TableView.showtable(Ωbus)),
        pad(1em,Interact.latex("\\mathbf{\\text{Lines status:}}")),
        pad(1em,TableView.showtable(Ωline)),
        pad(1em,Interact.latex("\\mathbf{\\text{Switches status:}}")),
        pad(1em,TableView.showtable(Ωkeys))
    )
    body!(w,ui)
end

function formatter(iter, best_cost, list_size, time)
    Ωstatus = DataFrame()
    Ωbus = DataFrame()
    Ωline =DataFrame()
    Ωkeys = DataFrame()
    (Ωbus.bar, Ωbus.name, Ωbus.type, Ωbus.Vphase, Ωbus.Vline, Ωbus.zone) = 
        (Ωb.num, Ωb.name, Ωb.bustype, Ωb.Vp, Ωb.Vl, Ωb.zb)
    (Ωline.from, Ωline.to, Ωline.Ikm, Ωline.Pkm, Ωline.Qkm, Ωline.Imax) =
        (Ωl.from, Ωl.to, Ωl.Ikm, Ωl.Pkm, Ωl.Qkm, Ωl.Imax)
    (Ωkeys.from, Ωkeys.to, Ωkeys.pos, Ωkeys.ini, Ωkeys.Ikm, Ωkeys.Pkm, Ωkeys.Qkm, Ωkeys.Imax) =
        (Ωsw.from, Ωsw.to, Ωsw.pos, Ωsw.ini, Ωsw.Ikm, Ωsw.Pkm, Ωsw.Qkm, Ωsw.Imax)
    
    id = 0
    voltage = Inf
    n_bars = 0
    zones = []
    zone_string =""
    for bar in eachrow(Ωb)
        if bar.Vl < voltage && bar.level != -1
        (id, voltage) = (bar.num, bar.Vl)
        end
        if bar.Vl == 0
            n_bars = n_bars+1
            if !(bar.zb in zones)
                push!(zones,bar.zb)
                if length(zones)==0
                    zone_string = string(bar.zb)
                else
                    zone_string = string(zone_string, bar.zb, ", ")
                end
            end
        end
    end
    print(zone_string)
    changes = sum(abs.(Ωsw.pos-Ωsw.ini))
    general = ["Bar with the lowest voltage","The lowest voltage","Active Power loss", "Neighbors", "Execution time"]
    value = [id,voltage,string(string(round(digits=2,loss()))," [kW]"), string(iter*length(eachrow(Ωsw))),string(time," s")]
    problem = ["Changes", "zones without energy", "number of bars without energy", "cost","Tabu List Size"]
    status = [changes, zone_string, n_bars, round(best_cost, digits=2), list_size]
    Ωstatus.general = general
    Ωstatus.value = value
    Ωstatus.problem = problem
    Ωstatus.status = status
    return Ωstatus, Ωbus, Ωline, Ωkeys
end
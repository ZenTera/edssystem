#Author: Lucas Zenichi Terada
#Institution: University of Campinas

#Define global variables
Ωb = Observable{Any}(DataFrame);
Ωl = Observable{Any}(DataFrame);
Ωd = Observable{Any}(DataFrame);
Ωsw = Observable{Any}(DataFrame);
Ωz =Observable{Any}(DataFrame);
Type = "None";

#Create the GUI Window
w = Blink.Window()
defaultpage()

#on(n -> powerflowpage(),PowerflowButton)
on(n -> defaultpage(),BackButton)
on(n -> select(),StartButton)


map!(CSV.read, Ωb, busbutton)
map!(CSV.read, Ωl, networkbutton)
map!(CSV.read, Ωsw,switchbutton)
map!(CSV.read, Ωd, databutton)
map!(CSV.read, Ωz, measuresbutton)

function shortcircuitpage()
    global Type = "Short Circuit"
    ui = vbox(
        hbox(pad(1em,description)),
        pad(1em,Interact.latex("\\text{Please insert the data files}")),
        pad(1em,busbutton),
        pad(1em,networkbutton),
        pad(1em,databutton),
        pad(1em,trafobox),
        pad(1em,trafobutton),
        pad(1em,shortcircuittype),
        hbox(pad(2em,BackButton),pad(2em,StartButton))
    )
    body!(w,ui);
end

function executeshortcircuit()
    global Ωb = DataFrame(Ωbars.val)
    global Ωl = DataFrame(Ωbranches.val)
    global Ωdata = DataFrame(Ωparameters.val)
    if trafobox[]
        global Ωtr = DataFrame(Ωtransformers.val)
    end
    if Ωdata.Type[1] == "PS"
        generators()
        Y = round.(digits=6,PSimpedance());
        shortcircuit(Y)
    elseif Ωdata.Type == "CS"
        print("Em construção")
    end
end
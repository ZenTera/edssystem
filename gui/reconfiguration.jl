function reconfigurationpage()
    global Type = "Reconfiguration"
    ui = vbox(
        hbox(pad(1em,description)),
        pad(1em,Interact.latex("\\text{Please insert the data files}")),
        pad(1em,busbutton),
        pad(1em,networkbutton),
        pad(1em,switchbutton),
        pad(1em,parbutton),
        hbox(pad(2em,BackButton),pad(2em,StartButton))
    )
    body!(w,ui);
end

function startreconfiguration()
    global Ωb = DataFrame(Ωbars.val);
    global Ωl = DataFrame(Ωbranches.val);
    global Ωsw = DataFrame(Ωswitches.val);
    global Ωsw.pos = Ωsw.ini;
    Ωl.r = Ωl.r/1000;
    Ωl.x = Ωl.x/1000;
    Ωsw.r = Ωsw.r/1000;
    Ωsw.x = Ωsw.x/1000;
    include(parbutton[])
    (radial, level, buslv, netlv, swtlv) = radiality()
    (V, Ibus, Inet, Iswt, diff, iteration) = sweeppowerflow(buslv, netlv, swtlv, level)
    states(V, Inet, Iswt)
    worst_cost = cost(cr, cch, cls)
    trace1 = scatter(x=1:length(eachrow(Ωb)), y=Ωb.Vl)
    trace2 = scatter(x=1:length(eachrow(Ωb)), y=Ωb.baseKV*Vmin)
    trace3 = scatter(x=1:length(eachrow(Ωb)), y=Ωb.baseKV*Vmax)
    g = plot([trace1,trace2,trace3])
    time = @elapsed begin
        (iter, best_cost, list_size) = reconf_tabusearch(BTmax, Vmax, Vmin)
    end
    print("TEMPO DE EXECUÇÃO: ", time)
    (Ωstatus, Ωbus, Ωline, Ωkeys) = formatter_reconfiguration(iter, worst_cost, best_cost, list_size, time)
    trace1 = scatter(x=1:length(eachrow(Ωb)), y=Ωb.Vl)
    trace2 = scatter(x=1:length(eachrow(Ωb)), y=Ωb.baseKV*Vmin)
    trace3 = scatter(x=1:length(eachrow(Ωb)), y=Ωb.baseKV*Vmax)
    p = plot([trace1,trace2,trace3])
    ui = vbox(
        hbox(pad(1em,description)),
        hbox(pad(1em,Interact.latex("\\text{Results of problem}")),pad(1em,BackButton)),
        pad(1em,Interact.latex("\\mathbf{\\text{Network status:}}")),
        pad(1em,TableView.showtable(Ωstatus)),
        pad(1em,Interact.latex("\\mathbf{\\text{Voltage profile before reconfiguration:}}")),
        g,
        pad(1em,Interact.latex("\\mathbf{\\text{Voltage profile after reconfiguration:}}")),
        p,
        pad(1em,Interact.latex("\\mathbf{\\text{Bus status:}}")),
        pad(1em,TableView.showtable(Ωbus)),
        pad(1em,Interact.latex("\\mathbf{\\text{Lines status:}}")),
        pad(1em,TableView.showtable(Ωline)),
        pad(1em,Interact.latex("\\mathbf{\\text{Switches status:}}")),
        pad(1em,TableView.showtable(Ωkeys))
    )
    body!(w,ui)
end

function formatter_reconfiguration(iter, worst_cost, best_cost, list_size, time)
    Ωstatus = DataFrame()
    Ωbus = DataFrame()
    Ωline =DataFrame()
    Ωkeys = DataFrame()
    (Ωbus.bar, Ωbus.name, Ωbus.type, Ωbus.Vphase, Ωbus.Vline) = 
        (Ωb.num, Ωb.name, Ωb.bustype, Ωb.Vp, Ωb.Vl)
    (Ωline.from, Ωline.to, Ωline.Ikm, Ωline.Pkm, Ωline.Qkm, Ωline.Imax) =
        (Ωl.from, Ωl.to, Ωl.Ikm, Ωl.Pkm, Ωl.Qkm, Ωl.Imax)
    (Ωkeys.from, Ωkeys.to, Ωkeys.pos, Ωkeys.ini, Ωkeys.Ikm, Ωkeys.Pkm, Ωkeys.Qkm, Ωkeys.Imax) =
        (Ωsw.from, Ωsw.to, Ωsw.pos, Ωsw.ini, Ωsw.Ikm, Ωsw.Pkm, Ωsw.Qkm, Ωsw.Imax)
    
    id = 0
    voltage = Inf
    for bar in eachrow(Ωb)
        if bar.Vl < voltage
            (id, voltage) = (bar.num, bar.Vl)
        end
    end
    changes = sum(abs.(Ωsw.pos-Ωsw.ini))
    general = ["Bar with the lowest voltage","The lowest voltage","Active Power loss", "Neighbors", "Execution time"]
    value = [id,voltage,string(string(round(digits=2,loss()))," [kW]"), string(iter*length(eachrow(Ωsw))),string(time," s")]
    problem = ["Changes", "zones without energy", "cost before reconfiguration", "cost after reconfiguration","Tabu List Size"]
    status = [changes, " ", round(worst_cost, digits=2), round(best_cost, digits=2), list_size]
    Ωstatus.general = general
    Ωstatus.value = value
    Ωstatus.problem = problem
    Ωstatus.status = status
    return Ωstatus, Ωbus, Ωline, Ωkeys
end
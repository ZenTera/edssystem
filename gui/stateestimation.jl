function estimationpage()
    global Type = "State Estimation"
    ui = vbox(
        hbox(pad(1em,description)),
        pad(1em,Interact.latex("\\text{Please insert the data files}")),
        pad(1em,busbutton),
        pad(1em,networkbutton),
        pad(1em,databutton),
        pad(1em,measuresbutton),
        pad(1em,statebutton),
        pad(1em,estimationtype),
        hbox(pad(2em,BackButton),pad(2em,StartButton))
    )
    body!(w,ui);
end

function executeestimation()
    global Ωb = DataFrame(Ωbars.val)
    global Ωz = DataFrame(Ωmeasures.val)
    global Ωl = DataFrame(Ωbranches.val)
    global Ωdata = DataFrame(Ωparameters.val)
    global Ωx = DataFrame(Ωvariables.val)
    time = @elapsed begin
        (Y,Κ) = admittance()
        (g,b,bsh) = parameters()
        if estimationtype[] == "Normal Equation"
            (V,θ,iterations,ϵ)=normalequation(Y,Κ,g,b,bsh)
        elseif estimationtype[] == "Sparse Tableau"
            (V,θ,iterations,ϵ)=sparsetableau(Y,Κ,g,b,bsh)
        end
        print("\nV: \n")
        display(V);
        print("\nθ: \n")
        display(rad2deg.(θ));
        print("\niteration: ",iterations,"\n")
        print("\nϵ: ",ϵ,"\n")
    end
    (Ωbus,Ωline,Ωstatus) = estimationoutput(V,θ,Y,Κ,ϵ,iterations,time)
    ui = vbox(
        hbox(pad(1em,description)),
        hbox(pad(1em,Interact.latex("\\text{Results of problem}")),pad(1em,BackButton)),
            pad(1em,Interact.latex("\\mathbf{\\text{Network status:}}")),
            pad(1em,TableView.showtable(Ωstatus)),
            pad(1em,Interact.latex("\\mathbf{\\text{Bus status:}}")),
            pad(1em,TableView.showtable(Ωbus)),
            pad(1em,Interact.latex("\\mathbf{\\text{Lines status:}}")),
            pad(1em,TableView.showtable(Ωline))
    )
    body!(w,ui);
end
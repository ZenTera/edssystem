#Author: Lucas Zenichi Terada
#Institution: University of Campinas

function radialpage()
    global Type = "Radial"
    ui = vbox(
        hbox(pad(1em,description)),
        pad(1em,Interact.latex("\\text{Please insert the data files}")),
        pad(1em,busbutton),
        pad(1em,networkbutton),
        pad(1em,parbutton),
        hbox(pad(2em,BackButton),pad(2em,StartButton))
    )
    body!(w,ui);
end

function executeradial()
    global Ωb = DataFrame(Ωbars.val);
    global Ωl = DataFrame(Ωbranches.val);
    global Ωsw = DataFrame();
    (Ωsw.num, Ωsw.from, Ωsw.to, Ωsw.r, Ωsw.x, Ωsw.pos) =([],[],[],[],[],[])
    Ωl.r = Ωl.r/1000;
    Ωl.x = Ωl.x/1000;
    include(parbutton[])

    time = @elapsed begin
        (radial, level, buslv, netlv, swtlv) = radiality()
        (V, Ibus, Inet, Iswt, diff, iteration) = sweeppowerflow(buslv, netlv, swtlv, level)
        states(V, Inet, Iswt)
    end
    (Ωstatus, Ωbus, Ωline) = radial_result(iteration, diff, time)
    trace1 = scatter(x=1:length(eachrow(Ωb)), y=Ωb.Vl)
    p = plot(trace1)
    ui = vbox(
        hbox(pad(1em,description)),
        hbox(pad(1em,Interact.latex("\\text{Results of problem}")),pad(1em,BackButton)),
        pad(1em,Interact.latex("\\mathbf{\\text{Network status:}}")),
        pad(1em,TableView.showtable(Ωstatus)),
        pad(1em,Interact.latex("\\mathbf{\\text{Voltage profile:}}")),
        p,
        pad(1em,Interact.latex("\\mathbf{\\text{Bus status:}}")),
        pad(1em,TableView.showtable(Ωbus)),
        pad(1em,Interact.latex("\\mathbf{\\text{Lines status:}}")),
        pad(1em,TableView.showtable(Ωline))
    )
    body!(w,ui)
end

function radial_result(iter, maxerror, time)
    Ωstatus = DataFrame()
    Ωbus = DataFrame()
    Ωline =DataFrame()
    (Ωbus.bar, Ωbus.name, Ωbus.type, Ωbus.Vphase, Ωbus.Vline) = 
        (Ωb.num, Ωb.name, Ωb.bustype, Ωb.Vp, Ωb.Vl)
    (Ωline.from, Ωline.to, Ωline.Ikm, Ωline.Pkm, Ωline.Qkm) =
        (Ωl.from, Ωl.to, Ωl.Ikm, Ωl.Pkm, Ωl.Qkm)
    id = 0
    voltage = Inf
    for bar in eachrow(Ωb)
        if bar.Vl < voltage
            (id, voltage) = (bar.num, bar.Vl)
        end
    end
    general = ["Bar with the lowest voltage","The lowest voltage","Active Power loss"]
    value = [id,voltage,string(string(round(digits=2,loss()))," [kW]")]
    status = ["Number of iterations", "Execution time", "Max error"]
    result = [string(iter), string(time," s"), maxerror]
    Ωstatus.general = general
    Ωstatus.value = value
    Ωstatus.status = status
    Ωstatus.result = result
    return Ωstatus, Ωbus, Ωline
end
#Author: Lucas Zenichi Terada
#Institution: University of Campinas

function defaultpage()
    global Ωb = Observable{Any}(DataFrame);
    global Ωl = Observable{Any}(DataFrame);
    global Ωd = Observable{Any}(DataFrame);
    global Ωsw = Observable{Any}(DataFrame);
    global Ωz = Observable{Any}(DataFrame);
    Type = "None"
    ui = vbox(
        hbox(pad(1em,description)), 
        pad(1em,Interact.latex("\\text{Please choose the type of procedure}")),
        hbox(pad(1em,PowerflowButton),
            pad(1em,Interact.latex("\\text{Select to solve power flow problems}"))),
        hbox(pad(1em,RadialButton),
            pad(1em,Interact.latex("\\text{Select to solve radial powerflow problems}"))),
        hbox(pad(1em,EstimationButton),
            pad(1em,Interact.latex("\\text{Select to solve state estimation problems}"))),
        # hbox(pad(1em,ShortcircuitButton),
        #     pad(1em,Interact.latex("\\text{Select to solve short circuit problems}"))),    
        hbox(pad(1em,RestorationButton),
            pad(1em,Interact.latex("\\text{Select to solve restoration problems}"))),
        hbox(pad(1em,ReconfigurationButton),
            pad(1em,Interact.latex("\\text{Select to solve reconfiguration problems}"))),
        hbox(pad(1em,CancelButton))    
    )
    body!(w, ui);
end

function select()
    if Type == "Power Flow"
        executepowerflow()
    elseif Type == "State Estimation"
        executeestimation()
    elseif Type == "Short Circuit"
        executeshortcircuit()
    elseif Type == "Restoration"
        startrestoration()
    elseif Type == "Reconfiguration"
        startreconfiguration()
    elseif Type == "Radial"
        executeradial()
    end
end
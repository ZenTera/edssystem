function estimationoutput(V,θ,Y,Κ,ϵ,iterations,time)
    #Create parameters vectors
    G = real(Y);
    B = imag(Y);
    lines_size = length(eachrow(Ωl));
    global Ωl.num = 1:lines_size;
    Ikm = zeros(ComplexF64,lines_size);
    Imk = zeros(ComplexF64,lines_size);
    Pkm = zeros(Float64,lines_size);
    Qkm = zeros(Float64,lines_size);
    Pmk = zeros(Float64,lines_size);
    Qmk = zeros(Float64,lines_size);
    Ploss = zeros(Float64,lines_size);
    Qloss = zeros(Float64,lines_size);
    Pk = zeros(Float64,length(eachrow(Ωb)));
    Qk = zeros(Float64,length(eachrow(Ωb)));

    #Create a answer Dataframes
    Ωbus = DataFrame();
    Ωline = DataFrame();
    Ωstatus = DataFrame();
   
    Ωbus.bar = Ωb.ob;
    Ωbus.Type = Ωb.Type;
    Ωbus.V = round.(V,digits=4);
    Ωbus.θ = round.(rad2deg.(θ),digits=2);
    for ob in eachrow(Ωb)
        k = ob.ob
        P = 0;
        for m in Κ[k]
            θkm = θ[k]-θ[m]
            P = P + V[k]*V[m]*(G[k,m]*cos(θkm)+B[k,m]*sin(θkm))
        end
        Pk[ob.ob] = P
        Q = 0;
        for m in Κ[k]
            θkm = θ[k]-θ[m]
            Q = Q + V[k]*V[m]*(G[k,m]*sin(θkm)-B[k,m]*cos(θkm))
        end
        Qk[ob.ob] = Q
    end
    Ωbus.Pk = Pk;
    Ωbus.Qk = Qk;
    Ωline.k = Ωl.from;
    Ωline.m = Ωl.to;
    for ol in eachrow(Ωl)
        k = ol.from;
        m = ol.to;
        ykm = 1/(ol.R+im*ol.X);
        gkm = real(ykm);
        bkm = imag(ykm)
        θkm = θ[k] - θ[m]
        Vkm = V[k]*V[m]
        Pkm[ol.num] = (gkm*(inv(ol.a)*V[k])^2-(inv(ol.a)*V[k])*V[m]*(gkm*cos(θkm)+bkm*sin(θkm)))Ωdata.Snom[1]
        Pmk[ol.num] = (gkm*V[m]^2-(inv(ol.a)*V[k])*V[m]*(gkm*cos(θkm)-bkm*sin(θkm)))*Ωdata.Snom[1]
        
        Qkm[ol.num] = (-(bkm+ol.bsh)*(inv(ol.a)*V[k])^2-(inv(ol.a)*V[k])*V[m]*(gkm*sin(θkm)-bkm*cos(θkm)))*Ωdata.Snom[1]
        Qmk[ol.num] = (-(bkm+ol.bsh)V[m]^2 + (inv(ol.a)*V[k])*V[m]*(gkm*sin(θkm)+bkm*cos(θkm)))*Ωdata.Snom[1]
        Ploss[ol.num] = Pkm[ol.num] + Pmk[ol.num];
        Qloss[ol.num] = Qkm[ol.num] + Qmk[ol.num];
    end
    Ωline.Pkm = round.(digits=2,Pkm);
    Ωline.Qkm = round.(digits=2,Qkm);
    Ωline.Pmk = round.(digits=2,Pmk);
    Ωline.Qmk = round.(digits=2,Qmk);
    Ωline.Ploss = round.(digits=2,Ploss);
    Ωline.Qloss = round.(digits=2,Qloss);
    (id,value) = getmin(Ωbus.V)
    name = ["Voltage unit","Angle unit","Apparent Power unit","True Power unit", "Reactive Power unit",
    "Bar with the lowest voltage"]
    status = ["pu","degree","kVA","kW","kVar",id]
    general = ["The lowest voltage","Active Power loss","Reactive Power loss",
                "Iterations","Max error","Execution time"]
    value = [value,string(string(round(digits=2,sum(Ωline.Ploss)))," [kW]"),
            string(string(round(digits=2,sum(Ωline.Qloss)))," [kVar]"),
            string(iterations),string(ϵ),string(time," s")]
    Ωstatus.name = name;
    Ωstatus.status = status;
    Ωstatus.general = general;
    Ωstatus.value = value;
    return Ωbus,Ωline,Ωstatus
end
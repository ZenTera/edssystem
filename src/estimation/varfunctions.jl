#Author: Lucas Zenichi Terada
#Institution: University of Campinas

function hPkm(oz,a,V,θ,g,b,bsh)
    k = oz.from;
    m = oz.to;
    θkm = θ[k]-θ[m];
    value = a[k,m]^2*V[k]^2*g[k,m]-a[k,m]*V[k]*V[m]*(g[k,m]*cos(θkm)+b[k,m]*sin(θkm))
    return value
end

function hQkm(oz,a,V,θ,g,b,bsh)
    k = oz.from;
    m = oz.to;
    θkm = θ[k]-θ[m];
    value = -a[k,m]^2*V[k]^2*(b[k,m]+bsh[k,m])-a[k,m]*V[k]*V[m]*(g[k,m]*sin(θkm)-b[k,m]*cos(θkm))
    return value
end

function hPk(oz,V,θ,G,B,Κ)
    k = oz.from;
    P = 0;
    for m in Κ[k]
        θkm = θ[k]-θ[m]
        P = P + V[k]*V[m]*(G[k,m]*cos(θkm)+B[k,m]*sin(θkm))
    end
    return P
end

function hQk(oz,V,θ,G,B,Κ)
    k = oz.from;
    Q = 0;
    for m in Κ[k]
        θkm = θ[k]-θ[m]
        Q = Q + V[k]*V[m]*(G[k,m]*sin(θkm)-B[k,m]*cos(θkm))
    end
    return Q
end

function updatevar(x)
    network_size = length(eachrow(Ωl));
    bars_size    = length(eachrow(Ωb));
    θ = zeros(Float64,bars_size);
    V = ones(Float64,bars_size);
    Pkm = zeros(Float64,network_size);
    Qkm = zeros(Float64,network_size);
    Pk = zeros(Float64,bars_size);
    Qk = zeros(Float64,bars_size);
    for ox in eachrow(Ωx)
        k = ox.from;
        m = ox.to;
        for ol in eachrow(Ωl)
            if ol.from == ox.from && ol.to == ox.to
                id = ol.num
            end
        end
        if ox.Type == "Pkm"
            Pkm[id] = x[ox.num];
        elseif ox.Type == "Qkm"
            Qkm[id] = x[ox.num];
        elseif ox.Type == "Pk"
            Pk[ox.from] = x[ox.num];
        elseif ox.Type == "Qk"
            Qk[ox.from] = x[ox.num];
        elseif ox.Type == "V"
            V[ox.from]  = x[ox.num];
        elseif ox.Type == "ang"
            θ[ox.from]  = x[ox.num];
        end
    end
    return Pkm,Qkm,Pk,Qk,V,θ
end
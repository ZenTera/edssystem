#Author: Lucas Zenichi Terada
#Institution: University of Campinas
function dPkmdθ(oz,ox,a,V,θ,g,b,bsh)
    k = oz.from;
    m = oz.to;
    θkm = θ[k]-θ[m];
    value = 0;
    if ox.from == oz.from
        value = a[k,m]*V[k]*V[m]*(g[k,m]*sin(θkm)-b[k,m]*cos(θkm))
    elseif ox.from == oz.to
        value = -a[k,m]*V[k]*V[m]*(g[k,m]*sin(θkm)-b[k,m]*cos(θkm))
    end
    return value
end

function dPkmdV(oz,ox,a,V,θ,g,b,bsh)
    k = oz.from;
    m = oz.to;
    θkm = θ[k]-θ[m];
    value = 0;
    if ox.from == oz.from
        value = 2*a[k,m]^2*V[k]*g[k,m]-a[k,m]*V[m]*(g[k,m]*cos(θkm)-b[k,m]*sin(θkm))
    elseif ox.from == oz.to
        value = -a[k,m]*V[k]*(g[k,m]*cos(θkm)+b[k,m]*sin(θkm))
    end
    return value
end

function dQkmdθ(oz,ox,a,V,θ,g,b,bsh)
    k = oz.from;
    m = oz.to;
    θkm = θ[k]-θ[m];
    value = 0;
    if ox.from == oz.from
        value = -a[k,m]*V[k]*V[m]*(b[k,m]*sin(θkm)+g[k,m]*cos(θkm))
    elseif ox.from == oz.to
        value = a[k,m]*V[k]*V[m]*(b[k,m]*sin(θkm)+g[k,m]*cos(θkm))
    end
    return value
end

function dQkmdV(oz,ox,a,V,θ,g,b,bsh)
    k = oz.from;
    m = oz.to;
    θkm = θ[k]-θ[m];
    value = 0;
    if ox.from == oz.from
        value = -2*a[k,m]^2*V[k]*(b[k,m]+bsh[k,m])+a[k,m]*V[m]*(b[k,m]*cos(θkm)-g[k,m]*sin(θkm))
    elseif ox.from == oz.to
        value = a[k,m]*V[k]*(b[k,m]*cos(θkm)-g[k,m]*sin(θkm))
    end
    return value
end

function dPkdθ(oz,ox,a,V,θ,G,B,Κ)
    k = oz.from;
    value = 0;
    if ox.from == oz.from
        for m in Κ[k]
            θkm = θ[k]-θ[m]
            value = value + V[k]*V[m]*(G[k,m]*sin(θkm)-B[k,m]*cos(θkm))
        end
        value = -value - B[k,k]*V[k]^2
    elseif ox.from != oz.from
        m = ox.from;
        θkm = θ[k]-θ[m];
        value = V[k]*V[m]*(G[k,m]*sin(θkm)-B[k,m]*cos(θkm))
    end
    return value
end

function dPkdV(oz,ox,a,V,θ,G,B,Κ)
    k = oz.from;
    value = 0;
    if ox.from == oz.from
        for m in Κ[k]
            θkm = θ[k]-θ[m]
            value = value + V[k]*V[m]*(G[k,m]*cos(θkm)+B[k,m]*sin(θkm))
        end
        value = (value+G[k,k]*V[k]^2)/V[k]
    elseif ox.from != oz.from
        m = ox.from;
        θkm = θ[k]-θ[m];
        value = V[k]*(G[k,m]*cos(θkm)+B[k,m]*sin(θkm))
    end
    return value
end

function dQkdθ(oz,ox,a,V,θ,G,B,Κ)
    k = oz.from;
    value = 0;
    if ox.from == oz.from
        for m in Κ[k]
            θkm = θ[k]-θ[m]
            value = value + V[k]*V[m]*(G[k,m]*cos(θkm)+B[k,m]*sin(θkm))
        end
        value = value - G[k,k]*V[k]^2
    elseif ox.from != oz.from
        m = ox.from;
        θkm = θ[k]-θ[m];
        value = -V[k]*V[m]*(G[k,m]*cos(θkm)+B[k,m]*sin(θkm))
    end
    return value
end

function dQkdV(oz,ox,a,V,θ,G,B,Κ)
    k = oz.from;
    value = 0;
    if ox.from == oz.from
        for m in Κ[k]
            θkm = θ[k]-θ[m]
            value = value + V[k]*V[m]*(G[k,m]*sin(θkm)-B[k,m]*cos(θkm))
        end
        value = (value-B[k,k]*V[k]^2)/V[k]
    elseif ox.from != oz.from
        m = ox.from;
        θkm = θ[k]-θ[m];
        value = V[k]*(G[k,m]*sin(θkm)-B[k,m]*cos(θkm))
    end
    return value
end

function dVdV(oz,ox)
    value = 0;
    if ox.from == oz.from
        value = 1;
    end
    return value
end

function dθdθ(oz,ox)
    value = 0;
    if ox.from == oz.from
        value = 1
    end
    return value
end



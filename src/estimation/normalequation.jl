#Author: Lucas Zenichi Terada
#Institution: University of Campinas
function trmatrix()
    size = length(eachrow(Ωb));
    a = zeros(Float64,size,size)
    for ol in eachrow(Ωl)
        a[ol.from,ol.to] = 1/ol.a;
        a[ol.to,ol.from] = ol.a
    end
    return a
end

function parameters()
    size = length(eachrow(Ωb));
    g = zeros(Float64,size,size);
    b = zeros(Float64,size,size);
    bsh = zeros(Float64,size,size);
    for ol in eachrow(Ωl)
        y = inv(ol.R+im*ol.X);
        g[ol.from,ol.to] = real(y);
        g[ol.to,ol.from] = real(y);
        b[ol.from,ol.to] = imag(y);
        b[ol.to,ol.from] = imag(y);
        bsh[ol.from,ol.to] = ol.bsh/2;
        bsh[ol.to,ol.from] = ol.bsh/2;
    end
    return (g,b,bsh)
end

function normalequation(Y,Κ,g,b,bsh)
    measure_size = length(eachrow(Ωz));
    state_size   = length(eachrow(Ωx));
    network_size = length(eachrow(Ωl));
    bars_size    = length(eachrow(Ωb));
    Rz = diagm(0 => Ωz.w.^(2));
    G = real(Y);
    B = imag(Y);
    H = zeros(Float64,measure_size,state_size);
    h = zeros(Float64,measure_size);
    x = zeros(Float64,state_size);
    #variables
    θ = zeros(Float64,bars_size);
    V = ones(Float64,bars_size);
    Pkm = zeros(Float64,network_size);
    Qkm = zeros(Float64,network_size);
    Pk = zeros(Float64,bars_size);
    Qk = zeros(Float64,bars_size);
    a = trmatrix();
    #Variables for state estimation
    Δx = zeros(Float64,state_size);
    ϵ = Inf;
    for ox in eachrow(Ωx)
        if ox.Type == "V"
            x[ox.num] = 1
        end
    end
    iterations = 0;
    while ϵ > 1e-3 && iterations < 10
        for oz in eachrow(Ωz)
            if oz.Type == "Pkm"
                h[oz.num] = hPkm(oz,a,V,θ,g,b,bsh); 
            elseif oz.Type == "Qkm"
                h[oz.num] = hQkm(oz,a,V,θ,g,b,bsh);
            elseif oz.Type == "Pk"
                h[oz.num] = hPk(oz,V,θ,G,B,Κ);
            elseif oz.Type == "Qk"
                h[oz.num] = hQk(oz,V,θ,G,B,Κ);
            elseif oz.Type == "ang"
                h[oz.num] = θ[oz.from]
            elseif oz.Type == "V"
                h[oz.num] = V[oz.from]
            end
            for ox in eachrow(Ωx)
                k = oz.from;
                m = oz.to;
                if oz.Type == "Pkm" && ox.Type == "ang"
                    H[oz.num,ox.num] = dPkmdθ(oz,ox,a,V,θ,g,b,bsh)
                elseif oz.Type == "Pkm" && ox.Type == "V"
                    H[oz.num,ox.num] = dPkmdV(oz,ox,a,V,θ,g,b,bsh)
                elseif oz.Type == "Qkm" && ox.Type == "ang"
                    H[oz.num,ox.num] = dQkmdθ(oz,ox,a,V,θ,g,b,bsh)
                elseif oz.Type == "Qkm" && ox.Type == "V"
                    H[oz.num,ox.num] = dQkmdV(oz,ox,a,V,θ,g,b,bsh)
                elseif oz.Type == "Pk" && ox.Type == "ang"
                    H[oz.num,ox.num] = dPkdθ(oz,ox,a,V,θ,G,B,Κ)
                elseif oz.Type == "Pk" && ox.Type == "V"
                    H[oz.num,ox.num] = dPkdV(oz,ox,a,V,θ,G,B,Κ)
                elseif oz.Type == "Qk" && ox.Type == "ang"
                    H[oz.num,ox.num] = dQkdθ(oz,ox,a,V,θ,G,B,Κ)
                elseif oz.Type == "Qk" && ox.Type == "V"
                    H[oz.num,ox.num] = dQkdV(oz,ox,a,V,θ,G,B,Κ)
                elseif oz.Type == "V" && ox.Type == "V"
                    H[oz.num,ox.num] = dVdV(oz,ox)
                elseif oz.Type == "ang" && ox.Type == "ang"
                    H[oz.num,ox.num] = dθdθ(oz,ox)
                end
            end
        end
        Gain = H'*inv(Rz)*H
        r = Ωz.z-h
        Δx = inv(Gain)*(H')*inv(Rz)*r
        x = x+Δx
        (Pkm,Qkm,Pk,Qk,V,θ) = updatevar(x);
        iterations = iterations+1
        ϵ = maximum(abs.(Δx))
        # print("iterations: ", iterations, "\n\n")
        # print("\nH:")
        # display(round.(digits=2,H))
        # print("\nh:")
        # display(round.(digits=2,h))
        # print("\nΔx: ")
        # display(round.(digits=6,Δx))
        # print("\nx: ")
        # display(round.(digits=6,x))
        # print("\nr:")
        # display(round.(digits=6,r))
        # print("\nϵ: ",ϵ,"\n")
    end
    return V,θ,iterations,ϵ
end
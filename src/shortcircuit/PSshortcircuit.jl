#Author: Lucas Zenichi Terada
#Institution: University of Campinas 
function matrixfault()
    # zf = 0.001+0im;
    # zg = 0.002+0im;
    zf =10/190.44 + 0im;
    zg =5;190.44 + 0im;
    y0 = 1/(zf+3*zg);
    yf = 1/zf;
    yg = 1/zg;
    ZF = 0;
    YF = 0;
    if shortcircuittype[] == "Three-phase"
        ZF = "Not defined";
        YF = (yf/3)*[2 -1 -1; -1 2 -1; -1 -1 2];
    elseif shortcircuittype[] == "Three-phase to ground"
        ZF = [zf+zg zg zg; zg zf+zg zg; zg zg zf+zg];
        YF = (1/3)*[y0+2*yf y0-yf y0-yf;y0-yf y0+2*yf y0-yf; y0-yf y0-yf y0+2*yf];
    elseif shortcircuittype[] == "Biphasic"
        ZF = "Not defined"
        YF = (yf/2)*[0 0 0; 0 1 -1; 0 -1 1];
    elseif shortcircuittype[] == "Biphasic to ground"
        ZF = [Inf 0 0; 0 zf+zg zg; 0 zg zf+zg];
        YF = inv((zf^2+2*zf*zg))*[0 0 0;0 zf+zg -zg; 0 -zg zf+zg];
    elseif shortcircuittype[] == "Single-phase"
        ZF = [zf 0 0; 0 Inf 0; 0 0 Inf];
        YF = [yf 0 0;0 0 0; 0 0 0];
    end
    return ZF,YF    
end

function dataout()
    line_size = length(eachrow(Ωl));
    Msize = length(eachrow(Ωb));
    Ifault  = zeros(ComplexF64,Msize,3);
    V  = zeros(ComplexF64,Msize,3);
    I  = zeros(ComplexF64,line_size,3);
    Vcases  = Array{DataFrame,1}(undef,Msize);
    Icases  = Array{DataFrame,1}(undef,Msize);
    V0  = zeros(ComplexF64,Msize,3);
    for ob in eachrow(Ωb)
        (VA,VB,VC,θA,θB,θC) = (ob.Va,ob.Vb,ob.Vc,ob.anga,ob.angb,ob.angc);
        V0[ob.ob,:] = [v(VA,θA);v(VB,θB);v(VC,θC)];
    end
    return V,I,Vcases, Icases, Ifault, Msize,V0
end

function resetdata()
    Ωcc   = DataFrame();
    Ωbus  = DataFrame();
    Ωline = DataFrame(); 
    Ωcc.bus    = Ωb.ob
    Ωbus.num   = Ωb.ob;
    Ωline.num  = Ωl.num;
    Ωline.k    = Ωl.from;
    Ωline.m    = Ωl.to;
    return Ωcc, Ωbus, Ωline
end

function v(v,θ)
    θ = deg2rad(θ)
    voltage = v*cos(θ) + im*v*sin(θ);
    return voltage
end

function shortcircuit(Y)
    Z = round.(digits=6,inv(Y));
    eye = [1+0im 0 0;0 1+0im 0; 0 0 1+0im];
    (ZF,YF) = matrixfault();
    (V,I,Vcases, Icases, Ifault, Msize, V0) = dataout();
    (Ωcc, Ωbus, Ωline) = resetdata();
    for ob in eachrow(Ωb)
        if ZF == "Not defined"
            (k,ki,kf) = (ob.ob,3*ob.ob-2,3*ob.ob);
                Ifault[k,:] = YF*inv(eye + Z[ki:kf,ki:kf]*YF)*V0[k,:];
        else
            (k,ki,kf) = (ob.ob,3*ob.ob-2,3*ob.ob);
            Ifault[ob.ob,:] = inv(ZF + Z[ki:kf,ki:kf])*V0[k,:];
        end
    end
    for kb in eachrow(Ωb)
        (k,ki,kf) = (kb.ob,3*kb.ob-2,3*kb.ob);
        V[k,1:3] = inv(eye + Z[ki:kf,ki:kf]*YF)*V0[k,:];
        for j in 1:Msize
            (ji,jf) = (3*j-2,3*j);
                if j != k
                V[j,:] = V0[j,:] - Z[ji:jf,ki:kf]*YF*inv(eye+Z[ki:kf,ki:kf]*YF)*V0[k,:]
            end
        end
        (Ωbus.VA,Ωbus.VB,Ωbus.VC) = (V[:,1],V[:,2],V[:,3]);
        display(Ωbus)
        Vcases[k] = Ωbus
        for ol in eachrow(Ωl)
            (p,q) = (ol.from,ol.to)
            (pi,pf,qi,qf) = (3*ol.from-2,3*ol.from,3*ol.to-2,3*ol.to)
            I[ol.num,1:3] = inv(Z[pi:pf,qi:qf])*(V[p,:]-V[q,:])
        end
        (Ωline.IAkm,Ωline.IBkm,Ωline.ICkm) = (I[:,1],I[:,2],I[:,3]);
        Icases[k] = Ωline
        V = zeros(ComplexF64,Msize,3);
        (Ωcc, Ωbus, Ωline) = resetdata();
    end
    (Ωcc.IfA,Ωcc.IfB,Ωcc.IfC) = (Ifault[:,1],Ifault[:,2],Ifault[:,3]);
    print("Correntes de falta nodais: \n")
    display(Ωcc)
    print("\n\n")
    print("Tensões nas barras: \n")
    display(Vcases)
    print("\n\n")
    print("Correntes nas linhas: \n")
    display(Icases)
    return Ωcc, Vcases, Icases, Ifault
end
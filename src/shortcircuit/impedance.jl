#Author: Lucas Zenichi Terada
#Institution: University of Campinas

function ret2pol(v)
    mod = sqrt(real(v)^2+imag(v)^2);
    ang = rad2deg(angle(v));
    return mod,ang
end

function impedance()
    size = length(eachrow(Ωb))
    Y = zeros(ComplexF64,length(eachrow(Ωb)),length(eachrow(Ωb)))
    for ob in eachrow(Ωb)
        k = ob.ob;
        if ob.Type == "SL" || ob.Type == "PV"
            Y[k,k] = inv(ob.R+im*ob.X);
        end
    end
    for ol in eachrow(Ωl)
        k = ol.from;
        m = ol.to;
        Y[k,k] = Y[k,k] + inv(ol.R+im*ol.X);
        Y[m,m] = Y[m,m] + inv(ol.R+im*ol.X);
        Y[k,m] = Y[k,m] - inv(ol.R+im*ol.X);
        Y[m,k] = Y[m,k] - inv(ol.R+im*ol.X);
    end
    Zpositive = inv(Y);
    return Zpositive
end

function impedancezero()
    size = length(eachrow(Ωb))
    Yzero = zeros(ComplexF64,length(eachrow(Ωb)),length(eachrow(Ωb)))
    for ob in eachrow(Ωb)
        k = ob.ob;
        if ob.Type == "SL" || ob.Type == "PV"
            Y[k,k] = inv(ob.R+im*ob.X);
        end
    end
    for ol in eachrow(Ωl)
        k = ol.from;
        m = ol.to;
        Y[k,k] = Y[k,k] + inv(ol.R+im*ol.X);
        Y[m,m] = Y[m,m] + inv(ol.R+im*ol.X);
        Y[k,m] = Y[k,m] - inv(ol.R+im*ol.X);
        Y[m,k] = Y[m,k] - inv(ol.R+im*ol.X);
    end
    Z = inv(Y);
    return Zpositive
end
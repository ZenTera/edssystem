function PS2Phase_ground(Y)
    Z = inv(Y);
    zf = 10+0im;
    zg = 15+0im;
    y0 = 1/(zf+3*zg);
    yf = 1/zf;
    Msize = length(eachrow(Ωb));
    (line_size,Ωcc,Ωbus,Ωline) = varoutput();
    Ifault  = zeros(ComplexF64,Msize,3);
    V  = zeros(ComplexF64,Msize,3);
    I  = zeros(ComplexF64,line_size,3);
    Vcases  = Array{DataFrame,1}(undef,Msize);
    Icases  = Array{DataFrame,1}(undef,Msize);
    Zf = [Inf 0 0; 0 zf+zg zg; 0 zg zf+zg];
    Yf = (zf^2+2*zf*zg)*[0 0 0;0 zf+zg -zg; 0 -zg zf+zg];
    for ob in eachrow(Ωb)
        ki = 3*ob.ob-2;
        kf = 3*ob.ob;
        Ifault[ob.ob,:] = inv(Zf + Z[ki:kf,ki:kf])*
                        [voltage(ob.Va,ob.anga);voltage(ob.Vb,ob.angb);voltage(ob.Vc,ob.angc)]
    end
    for kb in eachrow(Ωb)
        k = kb.ob
        for ib in eachrow(Ωb)
            i = ib.ob;
            V[i,1:3] = [voltage(ib.Va,ib.anga);voltage(ib.Vb,ib.angb);voltage(ib.Vc,ib.angc)]-
            Z[3*i-2:3*i,3*k-2:3*k]*Yf*inv([1.0+0im 0 0; 0 1.0+0im 0; 0 0 1.0+0im] + Z[3*k-2:3*k,3*k-2:3*k])*
            [voltage(kb.Va,kb.anga);voltage(kb.Vb,kb.angb);voltage(kb.Vc,kb.angc)]
        end
        Ωbus.VA = V[:,1];
        Ωbus.VB = V[:,2];
        Ωbus.VC = V[:,3];
        Vcases[k] = Ωbus
    end
    for kb in eachrow(Ωb)
        Ωaux = Vcases[kb.ob];
        for ol in eachrow(Ωl)
            pi = 3*ol.from-2;
            pf = 3*ol.from;
            qi = 3*ol.to-2;
            qf = 3*ol.to;
            I[ol.num,1:3] = inv(Z[pi:pf,qi:qf])*
                        ([Ωaux.VA[ol.from];Ωaux.VB[ol.from];Ωaux.VC[ol.from]]-
                         [Ωaux.VA[ol.to];Ωaux.VB[ol.to];Ωaux.VC[ol.to]])
        end
        Ωline.IAkm = I[:,1];
        Ωline.IBkm = I[:,2];
        Ωline.ICkm = I[:,3];
        Icases[kb.ob] = Ωline
    end

    return Ifault, Vcases, Icases
end

function PS2Phase(Y)
    Z = inv(Y);
    zf = 10+0im;
    yf = 1/zf;
    Msize = length(eachrow(Ωb));
    (line_size,Ωcc,Ωbus,Ωline) = varoutput();
    Ifault  = zeros(ComplexF64,Msize,3);
    V  = zeros(ComplexF64,Msize,3);
    I  = zeros(ComplexF64,line_size,3);
    Vcases  = Array{DataFrame,1}(undef,Msize);
    Icases  = Array{DataFrame,1}(undef,Msize);
    Yf = (yf/2)*[0 0 0; 0 1 -1; 0 -1 1];
    for ob in eachrow(Ωb)
        ki = 3*ob.ob-2;
        kf = 3*ob.ob;
        Ifault[ob.ob,:] = Yf*inv([1.0+0im 0 0; 0 1.0+0im 0; 0 0 1.0+0im] + Z[ki:kf,ki:kf]*Yf)*
                        [voltage(ob.Va,ob.anga);voltage(ob.Vb,ob.angb);voltage(ob.Vc,ob.angc)]
    end
    for kb in eachrow(Ωb)
        k = kb.ob
        for ib in eachrow(Ωb)
            i = ib.ob;
            if i==k
                V[i,1:3] = ([1.0+0im 0 0; 0 1.0+0im 0; 0 0 1.0+0im]+Z[3*i-2:3*i,3*i-2:3*i]*Yf)*
                [voltage(ib.Va,ib.anga);voltage(ib.Vb,ib.angb);voltage(ib.Vc,ib.angc)]
            else
                V[i,1:3] = [voltage(ib.Va,ib.anga);voltage(ib.Vb,ib.angb);voltage(ib.Vc,ib.angc)]-
                Z[3*i-2:3*i,3*k-2:3*k]*Yf*inv([1.0+0im 0 0; 0 1.0+0im 0; 0 0 1.0+0im] + Z[3*k-2:3*k,3*k-2:3*k])*
                [voltage(kb.Va,kb.anga);voltage(kb.Vb,kb.angb);voltage(kb.Vc,kb.angc)]
            end
        end
        Ωbus.VA = V[:,1];
        Ωbus.VB = V[:,2];
        Ωbus.VC = V[:,3];
        Vcases[k] = Ωbus
    end
    for kb in eachrow(Ωb)
        Ωaux = Vcases[kb.ob];
        for ol in eachrow(Ωl)
            pi = 3*ol.from-2;
            pf = 3*ol.from;
            qi = 3*ol.to-2;
            qf = 3*ol.to;
            I[ol.num,1:3] = inv(Z[pi:pf,qi:qf])*
                        ([Ωaux.VA[ol.from];Ωaux.VB[ol.from];Ωaux.VC[ol.from]]-
                         [Ωaux.VA[ol.to];Ωaux.VB[ol.to];Ωaux.VC[ol.to]])
        end
        Ωline.IAkm = I[:,1];
        Ωline.IBkm = I[:,2];
        Ωline.ICkm = I[:,3];
        Icases[kb.ob] = Ωline
    end
    return Ifault, Vcases, Icases
end
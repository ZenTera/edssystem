function generators()
    genpos = length(eachrow(Ωb))+1
    gensize = length(findall(x->x=="Gen", Ωb.Type))
    from = Int64[];
    to = Int64[];
    y0 = Float64[];
    y1 = Float64[];
    y2 = Float64[];
    for ob in eachrow(Ωb)
        if ob.Type == "Gen"
            push!(to,ob.ob);
            push!(from,genpos);
            push!(y0,ob.y0);
            push!(y1,ob.y1);
            push!(y2,ob.y2);
            genpos = genpos+1;
        end
    end
    global Ωgen.from = from;
    global Ωgen.to = to;
    global Ωgen.y0 = y0;
    global Ωgen.y1 = y1;
    global Ωgen.y2 = y2;
end

function Yline(ol)
    Zs = zeros(ComplexF64,3,3);
    Zs[1,1] = ol.raa + im*ol.xaa;
    Zs[2,2] = ol.rbb + im*ol.xbb;
    Zs[3,3] = ol.rcc + im*ol.xcc;
    Zs[1,2] = ol.rab + im*ol.xab;
    Zs[2,1] = ol.rba + im*ol.xba;
    Zs[1,3] = ol.rac + im*ol.xac;
    Zs[3,1] = ol.rca + im*ol.xca;
    Zs[2,3] = ol.rbc + im*ol.xbc;
    Zs[3,2] = ol.rcb + im*ol.xcb;
    Yl = inv(Zs)
    return Yl
end

function Yshunt(ol)
    Ysh = zeros(ComplexF64,3,3);
    Ysh[1,1] = im*ol.yaa;
    Ysh[2,2] = im*ol.ybb;
    Ysh[3,3] = im*ol.ycc;
    Ysh[1,2] = im*ol.yab;
    Ysh[2,1] = im*ol.yba;
    Ysh[1,3] = im*ol.yac;
    Ysh[3,1] = im*ol.yca;
    Ysh[2,3] = im*ol.ybc;
    Ysh[3,2] = im*ol.ycb;
    Ysh = Ysh/2
end

function Ytr(otr)
    yt = otr.gt + im*otr.bt
    α = 1+otr.tap1;
    β = 1+otr.tap2;
    Y1 = [yt 0 0; 0 yt 0; 0 0 yt];
    Y2 = (1/3)*[2*yt -yt -yt; -yt 2*yt -yt; -yt -yt 2*yt];
    Y3 = (1/sqrt(3))*[-yt yt 0; 0 -yt yt; yt 0 -yt];
    Y4 = (1/3)*[yt -yt 0; -yt 2*yt -yt; 0 -yt yt];
    Y5 = [yt 0 0; 0 yt 0; 0 0 0];
    Y6 = [-yt yt 0; 0 -yt yt; 0 0 0];
    if otr.pType == "Yg" && otr.sType == "Yg"
        Yp = Y1/α^2
        Ys = Y1/β^2
        Yps = -Y1/(α*β)
        Ysp = -Y1/(α*β)
    elseif otr.pType == "Yg" && otr.sType == "Y"
        Yp = Y2/α^2
        Ys = Y2/β^2
        Yps = -Y2/(α*β)
        Ysp = -Y2/(α*β)
    elseif otr.pType == "Yg" && otr.sType == "D"
        Yp = Y1/α^2
        Ys = Y2/β^2
        Yps = Y3/(α*β)
        Ysp = Y3'/(α*β)
    elseif otr.pType == "Y" && otr.sType == "Yg"
        Yp = Y2/α^2
        Ys = Y2/β^2
        Yps = -Y2/(α*β)
        Ysp = -Y2/(α*β)
    elseif otr.pType == "Y" && otr.sType == "Y"
        Yp = Y2/α^2
        Ys = Y2/β^2
        Yps = -Y2/(α*β)
        Ysp = -Y2/(α*β)
    elseif otr.pType == "Y" && otr.sType == "D"
        Yp = Y2/α^2
        Ys = Y2/β^2
        Yps = Y3/(α*β)
        Ysp = transpose(Y3)/(α*β)
    elseif otr.pType == "D" && otr.sType == "Yg"
        Yp = Y2/α^2
        Ys = Y1/β^2
        Yps = transpose(Y3)/(α*β)
        Ysp = Y3/(α*β)
    elseif otr.pType == "D" && otr.sType == "Y"
        Yp = Y2/α^2
        Ys = Y2/β^2
        Yps = Y3/(α*β)
        Ysp = transpose(Y3)/(α*β)
    elseif otr.pType == "D" && otr.sType == "D"
        Yp = Y2/α^2
        Ys = Y2/β^2
        Yps = -Y2/(α*β)
        Ysp = -Y2/(α*β)
    elseif otr.pType == "Da" && otr.sType == "Da"
        Yp = Y4/α^2
        Ys = Y4/β^2
        Yps = -Y4/(α*β)
        Ysp = -Y4/(α*β)
    elseif otr.pType == "Da" && otr.sType == "Yga"
        Yp = Y4/α^2
        Ys = Y5/β^2
        Yps = transpose(Y4)/(α*β)
        Ysp = Y4/(α*β)
    elseif otr.pType == "Yga" && otr.sType == "Yga"
        Yp = Y5/α^2
        Ys = Y5/β^2
        Yps = -Y5/(α*β)
        Ysp = -Y5/(α*β)
    end
    return Yp, Ys, Yps, Ysp
end

function Ygen(ogen)
    a = exp(im*2*π/3);
    T = [1 1 1; 1 a^2 a ; 1 a a^2];
    YG = inv([ogen.y0*im 0 0;0 ogen.y1*im 0; 0 0 ogen.y2*im])
    YG = T*YG*inv(T)
    YG = inv(YG)
    return YG
end

function Yload(ob)
    ya = (ob.Pa-im*ob.Qa)/(ob.Va^2*Ωdata.Snom[1]);
    yb = (ob.Pb-im*ob.Qb)/(ob.Vb^2*Ωdata.Snom[1]);
    yc = (ob.Pc-im*ob.Qc)/(ob.Vc^2*Ωdata.Snom[1]);
    Yload = [ya 0 0; 0 yb 0; 0 0 yc];
    return Yload
end

function PSimpedance()
    bus_size = length(eachrow(Ωb));
    gen_size = length(eachrow(Ωgen));
    matrix_size = 3*(bus_size+gen_size);
    Y = zeros(ComplexF64,matrix_size,matrix_size);
    for ol in eachrow(Ωl)
        ki = 3*ol.from-2;
        kf = 3*ol.from;
        mi = 3*ol.to-2;
        mf = 3*ol.to;
        Y[ki:kf,ki:kf] = Y[ki:kf,ki:kf] + Yshunt(ol) + Yline(ol);
        Y[mi:mf,mi:mf] = Y[mi:mf,mi:mf] + Yshunt(ol) + Yline(ol);
        Y[ki:kf,mi:mf] = - Yline(ol);
        Y[mi:mf,ki:kf] = - Yline(ol);
    end
    for ob in eachrow(Ωb)
        ki = 3*ob.ob-2;
        kf = 3*ob.ob;
        Y[ki:kf,ki:kf] = Y[ki:kf,ki:kf] + Yload(ob);
    end
    for ogen in eachrow(Ωgen)
        ki = 3*ogen.from-2;
        kf = 3*ogen.from;
        mi = 3*ogen.to-2;
        mf = 3*ogen.to;
        Y[ki:kf,ki:kf] = Y[ki:kf,ki:kf] + Ygen(ogen)
        Y[mi:mf,mi:mf] = Y[mi:mf,mi:mf] + Ygen(ogen)
        Y[ki:kf,mi:mf] = - Ygen(ogen)
        Y[mi:mf,ki:kf] = - Ygen(ogen)
    end
    if trafobox[] == true
        for otr in eachrow(Ωtr)
            ki = 3*otr.from-2;
            kf = 3*otr.from;
            mi = 3*otr.to-2;
            mf = 3*otr.to;
            (Yp,Ys,Yps,Ysp) = Ytr(otr);
            Y[ki:kf,ki:kf] = Y[ki:kf,ki:kf] + Yp;
            Y[mi:mf,mi:mf] = Y[mi:mf,mi:mf] + Ys;
            Y[ki:kf,mi:mf] = Yps;
            Y[mi:mf,ki:kf] = Ysp;
        end
    end
    return Y
end
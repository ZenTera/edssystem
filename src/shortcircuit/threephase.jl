#Author: Lucas Zenichi Terada
#Institution: University of Campinas

#Coluna: curto circuito na barra k
#Linha: Tensão no curto da barra e suas vizinhas
function threephase(Z)
    size = length(eachrow(Ωb))
    V = zeros(ComplexF64,size,size);
    I = zeros(ComplexF64,length(eachrow(Ωl)),size);
    Icc = zeros(ComplexF64,size);
    for ob in eachrow(Ωb)
        j = ob.ob
        Icc[j] = -1/Z[j,j];
        for i in 1:size
            V[i,j] = 1-Z[i,j]/Z[j,j];
        end
        for ol in eachrow(Ωl)
            k = ol.from;
            m = ol.to;
            I[ol.num,j] = (V[k,j]-V[m,j])/(ol.R+im*ol.X); 
        end
    end
    return V,I,Icc
end
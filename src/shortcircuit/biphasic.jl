#Author: Lucas Zenichi Terada
#Institution: University of Campinas

function biphasic(Zp)
    size = length(eachrow(Ωb))
    VA = ones(ComplexF64,size,size);
    VB = zeros(ComplexF64,size,size);
    VC = zeros(ComplexF64,size,size);
    IA = zeros(ComplexF64,length(eachrow(Ωl)),size);
    IB = zeros(ComplexF64,length(eachrow(Ωl)),size);
    IC = zeros(ComplexF64,length(eachrow(Ωl)),size);
    IAcc = zeros(ComplexF64,size);
    IBcc = zeros(ComplexF64,size);
    ICcc = zeros(ComplexF64,size);
    a = exp(im*2*π/3);
    #j bar that happend the fault
    #i the circuit bar
    for ob in eachrow(Ωb)
        j = ob.ob;
        IAcc[j] = 0;
        IBcc[j] = -im*sqrt(3)/(2*Zp[j,j]);
        ICcc[j] = im*sqrt(3)/(2*Zp[j,j]);
        for i in 1:size
            VA[i,j] = 1.0;
            VB[i,j] = a^2+im*sqrt(3)*(Zp[i,j]/(2*Zp[j,j]));
            VC[i,j] = a^2-im*sqrt(3)*(Zp[i,j]/(2*Zp[j,j]));
        end
        for ol in eachrow(Ωl)
            p = ol.from;
            q = ol.to;
            IA[ol.num,j] = 0; 
            IB[ol.num,j] = im*((Zp[p,j]-Zp[q,j])/(2*Zp[j,j]))*sqrt(3)/(ol.R+im*ol.X);
            IC[ol.num,j] = im*((Zp[q,j]-Zp[p,j])/(2*Zp[j,j]))*sqrt(3)/(ol.R+im*ol.X);
        end
        return VA,VB,VC,IA,IB,IC,IAcc,IBcc,ICcc
    end
end
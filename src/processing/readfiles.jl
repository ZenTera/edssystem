#Create the essentials DataFrames
Ωl = DataFrame()
Ωb = DataFrame()

#Read CSVs files
if setting.network != "no selected"
    Ωl = CSV.read(setting.network)
    Ωl.num = 1:length(eachrow(Ωl))
    Ωl.R = Ωl.R #Convert to kohms
    Ωl.X = Ωl.X #Convert to kohms
    Ωl.bsh = Ωl.bsh/2
end
if setting.bars != "no selected"
    Ωb = CSV.read(setting.bars)
    Ωb.P = Ωb.PG - Ωb.PD
    Ωb.Q = Ωb.QG - Ωb.QD
end
if setting.switches != "no selected"
    Ωch = CSV.read(setting.switches)
    Ωch.pos = Ωch.ini
end
if setting.zones != "no selected"
    Ωz = DataFrame()
    Ωz = CSV.read(setting.zones)
end
if setting.data != "no selected"
    Ωdat = CSV.read(setting.data)
end
if setting.trafo != "no selected"
    Ωtr = CSV.read(setting.trafo)
end

#Author: Lucas Zenichi Terada
#Institution: University of Campinas
#Description:

#Create the input and output lines
mutable struct LINES
    in::Any
    out::Any
end

#Create the parameters to line
Κ = LINES([],[])        #network lines
Λ = LINES([],[])        #switches lines

for i in Ωb.ob
    inlines = Int64[]
    outlines = Int64[]
    for ol in eachrow(Ωl)
        if ol.to == i
            push!(inlines, ol.num)
        end 
        if ol.from == i
            push!(outlines,ol.num)
        end
    end
    push!(Κ.in,inlines)
    push!(Κ.out,outlines)
    if @isdefined Ωch
        inlines = Int64[]
        outlines = Int64[]
        for och in eachrow(Ωch)
            if och.to == i
                push!(inlines, och.ow)
            end 
            if och.from == i
                push!(outlines,och.ow)
            end
        end
        push!(Λ.in,inlines)
        push!(Λ.out,outlines)
    end
end
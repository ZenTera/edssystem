function activepower(Pcalc,size,Κ,V,θ,B,G)
    for k in 1:size
        Pcalc[k] = 0;
        for m in Κ[k]
            θkm = θ[k] - θ[m];
            Pcalc[k] = Pcalc[k] + V[k]*V[m]*(G[k,m]*cos(θkm)+B[k,m]*sin(θkm));
        end
    end
    return Pcalc
end

function reactivepower(Qcalc,size,Κ,V,θ,B,G)
    for k in 1:size
        Qcalc[k] = 0;
        for m in Κ[k]
            θkm = θ[k] - θ[m];
            Qcalc[k] = Qcalc[k] + V[k]*V[m]*(G[k,m]*sin(θkm)-B[k,m]*cos(θkm));
        end
    end
    return Qcalc
end

function activeerror(ΔP)
    size = length(eachrow(Ωb))
    Pϵ = zeros(Float64,size)
    for ob in eachrow(Ωb)
        if ob.Type == "SL"
            Pϵ[ob.ob] = 0
        elseif ob.Type == "PV"
            Pϵ[ob.ob] = abs(ΔP[ob.ob])
        elseif ob.Type == "PQ"
            Pϵ[ob.ob] = abs(ΔP[ob.ob])
        end
    end
    ϵmax = maximum(Pϵ)
    return ϵmax
end

function reactiveerror(ΔQ)
    size = length(eachrow(Ωb))
    Qϵ = zeros(Float64,size)
    for ob in eachrow(Ωb)
        if ob.Type == "SL"
            Qϵ[ob.ob] = 0
        elseif ob.Type == "PV"
            Qϵ[ob.ob] = 0
        elseif ob.Type == "PQ"
            Qϵ[ob.ob] = abs(ΔQ[ob.ob])
        end
    end
    ϵmax = maximum(Qϵ)
    return ϵmax
end

function alternate(Y,Κ)
    global Ωb.P = (Ωb.PG - Ωb.PD)/Ωdata.Snom[1]
    global Ωb.Q = (Ωb.QG - Ωb.QD)/Ωdata.Snom[1]
    size = length(eachrow(Ωb))
    G = real(Y);
    B = imag(Y);
    #Create the Jacobian Submatrices
    size = length(eachrow(Ωb))
    θ = zeros(Float64,size)
    θ = setangle(θ);
    V = convert(Array,Ωb.Tb)
    Pcalc = zeros(Float64,size)
    Qcalc = zeros(Float64,size)
    H = zeros(Float64,size,size)
    L = zeros(Float64,size,size)
    KP = 1; KQ = 1; p = 0; q = 0;

    iterations = 0;

    while iterations < 300
        Pcalc = activepower(Pcalc,size,Κ,V,θ,B,G);
        ΔP = (Ωb.ap + Ωb.bp.*V + Ωb.cp.*V.*V).*(Ωb.P) - Pcalc
        if activeerror(ΔP) < 1e-4
            KP = 0;
            if KQ == 0
                break;        
            end
        else
            for k in 1:size
                if Ωb.Type[k] == "SL"
                    H[k,k] = Inf
                elseif Ωb.Type[k] == "PV"
                    H[k,k] = -B[k,k]*V[k]^2 - Qcalc[k]
                elseif Ωb.Type[k] == "PQ"
                    H[k,k] = -B[k,k]*V[k]^2 - Qcalc[k]
                end
            end
            for ol in eachrow(Ωl)
                k = ol.from;
                m = ol.to;
                θkm = θ[k] - θ[m]
                H[k,m] =  V[k]*V[m]*(G[k,m]*sin(θkm)-B[k,m]*cos(θkm))
                H[m,k] = -V[k]*V[m]*(G[k,m]*sin(θkm)+B[k,m]*cos(θkm))           
            end
            if typeof(Ωsw) == DataFrame
                for osw in eachrow(Ωsw)
                    if osw.pos == 1
                        k = osw.from;
                        m = osw.to;
                        θkm = θ[k] - θ[m]
                        H[k,m] =  V[k]*V[m]*(G[k,m]*sin(θkm)-B[k,m]*cos(θkm))
                        H[m,k] = -V[k]*V[m]*(G[k,m]*sin(θkm)+B[k,m]*cos(θkm))
                    end           
                end
            end
            θ = θ + inv(H)*ΔP;
            p = p+1;
            KQ = 1;
        end
        Qcalc = reactivepower(Qcalc,size,Κ,V,θ,B,G);
        ΔQ = (Ωb.aq + Ωb.bq.*V + Ωb.cq.*V.*V).*(Ωb.Q) - Qcalc;
        if reactiveerror(ΔQ) < 1e-4
            KQ = 0;
            if KP == 0
                break
            end
        else
            for k in 1:size
                if Ωb.Type[k] == "SL"
                    L[k,k] = Inf
                elseif Ωb.Type[k] == "PV"
                    L[k,k] = Inf
                elseif Ωb.Type[k] == "PQ"
                    L[k,k] = -(Ωb.bq[k] + 2*Ωb.cq[k]*V[k])*Ωb.Q[k] + (Qcalc[k] - B[k,k]*V[k]^2)/V[k]        
                end
            end
            for ol in eachrow(Ωl)
                k = ol.from;
                m = ol.to;
                θkm = θ[k] - θ[m]
                L[k,m] =  V[k]*(G[k,m]*sin(θkm)-B[k,m]*cos(θkm))
                L[m,k] = -V[m]*(G[k,m]*sin(θkm)+B[k,m]*cos(θkm))            
            end
            if typeof(Ωsw) == DataFrame
                for osw in eachrow(Ωsw)
                    if osw.pos == 1
                        k = osw.from;
                        m = osw.to;
                        θkm = θ[k] - θ[m]
                        L[k,m] =  V[k]*(G[k,m]*sin(θkm)-B[k,m]*cos(θkm))
                        L[m,k] = -V[m]*(G[k,m]*sin(θkm)+B[k,m]*cos(θkm))      
                    end           
                end
            end
            V = V + inv(L)*ΔQ;
            q = q+1;
            KP = 1;
        end
        iterations = iterations+1
    end
    ϵ = maximum([activeerror(Ωb.P-Pcalc);reactiveerror(Ωb.Q-Qcalc)])
    # print("Iterações ativas: ",p,"\n\n")
    # print("Iterações reativas: ",q,"\n\n")
    return V,θ,Pcalc,Qcalc,ϵ,iterations
end
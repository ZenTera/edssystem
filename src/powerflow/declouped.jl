#Author: Lucas Zenichi Terada
#Institution: Lucas Zenichi Terada
#Description:
include("admittance.jl")

function declouped(Y,Κ)
    global Ωb.P = (Ωb.PG - Ωb.PD)/Ωdata.Snom[1]
    global Ωb.Q = (Ωb.QG - Ωb.QD)/Ωdata.Snom[1]
    size = length(eachrow(Ωb))
    G = real(Y);
    B = imag(Y);
    ϵ = Inf;
    #Create the Jacobian Submatrices
    size = length(eachrow(Ωb))
    θ = zeros(Float64,size)
    θ = setangle(θ);
    V = convert(Array,Ωb.Tb)
    Pcalc = zeros(Float64,size)
    Qcalc = zeros(Float64,size)
    H = zeros(Float64,size,size)
    L = zeros(Float64,size,size)

    iterations = 0;

    ############################################################################################
    for k in 1:size
        Pcalc[k] = 0;
        Qcalc[k] = 0;
        for m in Κ[k]
            θkm = θ[k] - θ[m];
            Pcalc[k] = Pcalc[k] + V[k]*V[m]*(G[k,m]*cos(θkm)+B[k,m]*sin(θkm));
            Qcalc[k] = Qcalc[k] + V[k]*V[m]*(G[k,m]*sin(θkm)-B[k,m]*cos(θkm));
        end
    end
    #############################################################################################
    while ϵ > 1e-4 && iterations < 50
        iterations = iterations+1;
        for k in 1:size
            if Ωb.Type[k] == "SL"
                H[k,k] = Inf
                L[k,k] = Inf
            elseif Ωb.Type[k] == "PV"
                L[k,k] = Inf
                H[k,k] = -B[k,k]*V[k]^2 - Qcalc[k]
            elseif Ωb.Type[k] == "PQ"
                H[k,k] = -B[k,k]*V[k]^2 - Qcalc[k]
                L[k,k] = -(Ωb.bq[k] + 2*Ωb.cq[k]*V[k])*Ωb.Q[k] + (Qcalc[k] - B[k,k]*V[k]^2)/V[k]        
            end
        end
        for ol in eachrow(Ωl)
            k = ol.from;
            m = ol.to;
            θkm = θ[k] - θ[m]
            H[k,m] =  V[k]*V[m]*(G[k,m]*sin(θkm)-B[k,m]*cos(θkm))
            H[m,k] = -V[k]*V[m]*(G[k,m]*sin(θkm)+B[k,m]*cos(θkm))
            L[k,m] =  V[k]*(G[k,m]*sin(θkm)-B[k,m]*cos(θkm))
            L[m,k] = -V[m]*(G[k,m]*sin(θkm)+B[k,m]*cos(θkm))            
        end
        if typeof(Ωsw) == DataFrame
            for osw in eachrow(Ωsw)
                if osw.pos == 1
                    k = osw.from;
                    m = osw.to;
                    θkm = θ[k] - θ[m]
                    H[k,m] =  V[k]*V[m]*(G[k,m]*sin(θkm)-B[k,m]*cos(θkm))
                    H[m,k] = -V[k]*V[m]*(G[k,m]*sin(θkm)+B[k,m]*cos(θkm))
                    L[k,m] =  V[k]*(G[k,m]*sin(θkm)-B[k,m]*cos(θkm))
                    L[m,k] = -V[m]*(G[k,m]*sin(θkm)+B[k,m]*cos(θkm))
                end            
            end
        end

        if det(H) == 0 || det(L) == 0
            print("Singular\n")
            break
        end
        
        ΔP = (Ωb.ap + Ωb.bp.*V + Ωb.cp.*V.*V).*(Ωb.P) - Pcalc;
        ΔQ = (Ωb.aq + Ωb.bq.*V + Ωb.cq.*V.*V).*(Ωb.Q) - Qcalc;
        
        θ = θ + inv(H)*ΔP;
        V = V + inv(L)*ΔQ;

        ############################################################################################
        for k in 1:size
            Pcalc[k] = 0;
            Qcalc[k] = 0;
            for m in Κ[k]
                θkm = θ[k] - θ[m];
                Pcalc[k] = Pcalc[k] + V[k]*V[m]*(G[k,m]*cos(θkm)+B[k,m]*sin(θkm));
                Qcalc[k] = Qcalc[k] + V[k]*V[m]*(G[k,m]*sin(θkm)-B[k,m]*cos(θkm));
            end
        end
        ##############################################################################################
        ΔP = (Ωb.ap + Ωb.bp.*V + Ωb.cp.*V.*V).*(Ωb.P) - Pcalc;
        ΔQ = (Ωb.aq + Ωb.bq.*V + Ωb.cq.*V.*V).*(Ωb.Q) - Qcalc;
        ϵ = error(ΔP,ΔQ)
    end
    return V,θ,Pcalc,Qcalc,ϵ,iterations
end
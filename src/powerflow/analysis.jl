#Author: Lucas Zenichi Terada
#Institution: University of Campinas
#Description:

function polar(v,ϕ)
    v = v*cos(ϕ) + im*v*sin(ϕ)
    return v 
end

function ret(v)
    v = sqrt(real(v)^2 + imag(v)^2)
    ϕ = angle(v)
    return v,ϕ
end

function getmin(v)
    value = Inf;
    id = 0;
    for i in 1:length(v)
        if v[i] < value
            value = v[i];
            id = i;
        end
    end
    id = string(id)
    value = string(round(digits=4,value)," [pu]")
    return id,value
end

function analysis(V,θ,Pcalc,Qcalc,ϵ,iterations,time)
    #Create parameters vectors
    lines_size = length(eachrow(Ωl));
    global Ωl.num = 1:lines_size;
    Ikm = zeros(ComplexF64,lines_size);
    Imk = zeros(ComplexF64,lines_size);
    Pkm = zeros(Float64,lines_size);
    Qkm = zeros(Float64,lines_size);
    Pmk = zeros(Float64,lines_size);
    Qmk = zeros(Float64,lines_size);
    Ploss = zeros(Float64,lines_size);
    Qloss = zeros(Float64,lines_size);

    #Create a answer Dataframes
    Ωbus = DataFrame();
    Ωline = DataFrame();
    Ωstatus = DataFrame();
   
    Ωbus.bar = Ωb.ob;
    Ωbus.Type = Ωb.Type;
    Ωbus.V = round.(V,digits=4);
    Ωbus.θ = round.(rad2deg.(θ),digits=2);
    Ωbus.Pload = Ωb.PD;
    Ωbus.Qload = Ωb.QD;
    Ωbus.Pgen = round.(Pcalc*Ωdata.Snom[1] + Ωb.PD, digits=2);
    Ωbus.Qgen = round.(Qcalc*Ωdata.Snom[1] + Ωb.QD, digits=2);

    Ωline.k = Ωl.from;
    Ωline.m = Ωl.to;
    for ol in eachrow(Ωl)
        k = ol.from;
        m = ol.to;
        ykm = 1/(ol.R+im*ol.X);
        gkm = real(ykm);
        bkm = imag(ykm)
        θkm = θ[k] - θ[m]
        Vkm = V[k]*V[m]
        Pkm[ol.num] = (gkm*(inv(ol.a)*V[k])^2-(inv(ol.a)*V[k])*V[m]*(gkm*cos(θkm)+bkm*sin(θkm)))Ωdata.Snom[1]
        Pmk[ol.num] = (gkm*V[m]^2-(inv(ol.a)*V[k])*V[m]*(gkm*cos(θkm)-bkm*sin(θkm)))*Ωdata.Snom[1]
        
        Qkm[ol.num] = (-(bkm+ol.bsh)*(inv(ol.a)*V[k])^2-(inv(ol.a)*V[k])*V[m]*(gkm*sin(θkm)-bkm*cos(θkm)))*Ωdata.Snom[1]
        Qmk[ol.num] = (-(bkm+ol.bsh)V[m]^2 + (inv(ol.a)*V[k])*V[m]*(gkm*sin(θkm)+bkm*cos(θkm)))*Ωdata.Snom[1]
        Ploss[ol.num] = Pkm[ol.num] + Pmk[ol.num];
        Qloss[ol.num] = Qkm[ol.num] + Qmk[ol.num];
    end
    #Ωline.Ikm = Ikm;
    #Ωline.Imk = Imk;
    Ωline.Pkm = round.(digits=2,Pkm);
    Ωline.Qkm = round.(digits=2,Qkm);
    Ωline.Pmk = round.(digits=2,Pmk);
    Ωline.Qmk = round.(digits=2,Qmk);
    Ωline.Ploss = round.(digits=2,Ploss);
    Ωline.Qloss = round.(digits=2,Qloss);

    print(Ωbus,'\n');
    print(Ωline,'\n');

    (id,value) = getmin(Ωbus.V)
    name = ["Voltage unit","Angle unit","Apparent Power unit","True Power unit", "Reactive Power unit",
    "Bar with the lowest voltage"]
    status = ["pu","degree","kVA","kW","kVar",id]
    general = ["The lowest voltage","Active Power loss","Reactive Power loss",
                "Iterations","Max error","Execution time"]
    value = [value,string(string(round(digits=2,sum(Ωline.Ploss)))," [kW]"),
            string(string(round(digits=2,sum(Ωline.Qloss)))," [kVar]"),
            string(iterations),string(ϵ),string(time," s")]
    Ωstatus.name = name;
    Ωstatus.status = status;
    Ωstatus.general = general;
    Ωstatus.value = value;
    return Ωbus,Ωline,Ωstatus
end

function keyanalysis(V,θ)
    Ωkeys = DataFrame();
    #Create parameters vectors
    lines_size = length(eachrow(Ωsw));
    global Ωsw.num = 1:lines_size;
    Ikm = zeros(ComplexF64,lines_size);
    Imk = zeros(ComplexF64,lines_size);
    Pkm = zeros(Float64,lines_size);
    Qkm = zeros(Float64,lines_size);
    Pmk = zeros(Float64,lines_size);
    Qmk = zeros(Float64,lines_size);
    Ploss = zeros(Float64,lines_size);
    Qloss = zeros(Float64,lines_size);

    Ωkeys.k = Ωsw.from;
    Ωkeys.m = Ωsw.to;
    for osw in eachrow(Ωsw)
        k = osw.from;
        m = osw.to;
        ykm = 1/(osw.R+im*osw.X);
        gkm = real(ykm);
        bkm = imag(ykm)
        θkm = θ[k] - θ[m]
        Vkm = V[k]*V[m]
        Pkm[osw.num] = (gkm*(inv(osw.a)*V[k])^2-(inv(osw.a)*V[k])*V[m]*(gkm*cos(θkm)+bkm*sin(θkm)))Ωdata.Snom[1]
        Pmk[osw.num] = (gkm*V[m]^2-(inv(osw.a)*V[k])*V[m]*(gkm*cos(θkm)-bkm*sin(θkm)))*Ωdata.Snom[1]
        
        Qkm[osw.num] = (-(bkm+osw.bsh)*(inv(osw.a)*V[k])^2-(inv(osw.a)*V[k])*V[m]*(gkm*sin(θkm)-bkm*cos(θkm)))*Ωdata.Snom[1]
        Qmk[osw.num] = (-(bkm+osw.bsh)V[m]^2 + (inv(osw.a)*V[k])*V[m]*(gkm*sin(θkm)+bkm*cos(θkm)))*Ωdata.Snom[1]
        Ploss[osw.num] = Pkm[osw.num] + Pmk[osw.num];
        Qloss[osw.num] = Qkm[osw.num] + Qmk[osw.num];
    end
    #Ωline.Ikm = Ikm;
    #Ωline.Imk = Imk;
    Ωkeys.Pkm = round.(digits=2,Pkm);
    Ωkeys.Qkm = round.(digits=2,Qkm);
    Ωkeys.Pmk = round.(digits=2,Pmk);
    Ωkeys.Qmk = round.(digits=2,Qmk);
    Ωkeys.Ploss = round.(digits=2,Ploss);
    Ωkeys.Qloss = round.(digits=2,Qloss);
    print(Ωkeys)
    return Ωkeys

end
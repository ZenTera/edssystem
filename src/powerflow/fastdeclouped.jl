function activepower(Pcalc,size,Κ,V,θ,B,G)
    for k in 1:size
        Pcalc[k] = 0;
        for m in Κ[k]
            θkm = θ[k] - θ[m];
            Pcalc[k] = Pcalc[k] + V[k]*V[m]*(G[k,m]*cos(θkm)+B[k,m]*sin(θkm));
        end
    end
    return Pcalc
end

function reactivepower(Qcalc,size,Κ,V,θ,B,G)
    for k in 1:size
        Qcalc[k] = 0;
        for m in Κ[k]
            θkm = θ[k] - θ[m];
            Qcalc[k] = Qcalc[k] + V[k]*V[m]*(G[k,m]*sin(θkm)-B[k,m]*cos(θkm));
        end
    end
    return Qcalc
end

function activeerror(ΔP)
    size = length(eachrow(Ωb))
    Pϵ = zeros(Float64,size)
    for ob in eachrow(Ωb)
        if ob.Type == "SL"
            Pϵ[ob.ob] = 0
        elseif ob.Type == "PV"
            Pϵ[ob.ob] = abs(ΔP[ob.ob])
        elseif ob.Type == "PQ"
            Pϵ[ob.ob] = abs(ΔP[ob.ob])
        end
    end
    ϵmax = maximum(Pϵ)
    return ϵmax
end

function reactiveerror(ΔQ)
    size = length(eachrow(Ωb))
    Qϵ = zeros(Float64,size)
    for ob in eachrow(Ωb)
        if ob.Type == "SL"
            Qϵ[ob.ob] = 0
        elseif ob.Type == "PV"
            Qϵ[ob.ob] = 0
        elseif ob.Type == "PQ"
            Qϵ[ob.ob] = abs(ΔQ[ob.ob])
        end
    end
    ϵmax = maximum(Qϵ)
    return ϵmax
end

function fastdeclouped(Y,Κ)
    global Ωb.P = (Ωb.PG - Ωb.PD)/Ωdata.Snom[1]
    global Ωb.Q = (Ωb.QG - Ωb.QD)/Ωdata.Snom[1]
    size = length(eachrow(Ωb))
    G = real(Y);
    B = imag(Y);
    #Create the Jacobian Submatrices
    size = length(eachrow(Ωb))
    θ = zeros(Float64,size)
    θ = setangle(θ);
    V = convert(Array,Ωb.Tb)
    Pcalc = zeros(Float64,size)
    Qcalc = zeros(Float64,size)
    B1 = zeros(Float64,size,size)
    B2 = zeros(Float64,size,size)
    KP = 1; KQ = 1; p = 0; q = 0;

    iterations = 0;

    #Create the Bs matrix
    for ob in eachrow(Ωb)
        k = ob.ob
        if ob.Type == "SL"
            B1[k,k] = Inf;
            B2[k,k] = Inf;
        elseif ob.Type == "PV"
            B2[k,k] = Inf;
            temp = 0;
            for ol in eachrow(Ωl)
                if k == ol.from || k == ol.to
                    temp = temp + 1/ol.X
                end
            end
            if typeof(Ωsw) == DataFrame
                for osw in eachrow(Ωsw)
                    if k == osw.from || k == osw.to
                        if osw.pos == true
                            temp = temp + 1/osw.X
                        end
                    end
                end
            end
            B1[k,k] = temp;
        elseif ob.Type == "PQ"
            temp = 0;
            for ol in eachrow(Ωl)
                if k == ol.from || k == ol.to
                    temp = temp + 1/ol.X
                end
            end
            if typeof(Ωsw) == DataFrame
                for osw in eachrow(Ωsw)
                    if k == osw.from || k == osw.to
                        if osw.pos == true
                            temp = temp + 1/osw.X
                        end
                    end
                end
            end
            B1[k,k] = temp;
            B2[k,k] = -B[k,k];
        end
    end
    for ol in eachrow(Ωl)
        B1[ol.from,ol.to] = -1/ol.X
        B1[ol.to,ol.from] = -1/ol.X
        B2[ol.from,ol.to] = -B[ol.from,ol.to]
        B2[ol.to,ol.from] = -B[ol.from,ol.to]
    end
    if typeof(Ωsw) == DataFrame
        for osw in eachrow(Ωsw)
            if osw.pos == true
                B1[osw.from,osw.to] = -1/osw.X
                B1[osw.to,osw.from] = -1/osw.X
                B2[osw.from,osw.to] = -B[osw.from,osw.to]
                B2[osw.to,osw.from] = -B[osw.from,osw.to]
            end
        end
    end

    invB1 = inv(B1);
    invB2 = inv(B2);

    while iterations < 200
        Pcalc = activepower(Pcalc,size,Κ,V,θ,B,G);
        ΔP = (Ωb.ap + Ωb.bp.*V + Ωb.cp.*V.*V).*(Ωb.P) - Pcalc
        if activeerror(ΔP) < 1e-4
            KP = 0;
            if KQ == 0
                break;        
            end
        else
            θ = θ + invB1*(ΔP./V);
            p = p+1;
            KQ = 1;
        end
        Qcalc = reactivepower(Qcalc,size,Κ,V,θ,B,G);
        ΔQ = (Ωb.aq + Ωb.bq.*V + Ωb.cq.*V.*V).*(Ωb.Q) - Qcalc;
        if reactiveerror(ΔQ) < 1e-4
            KQ = 0;
            if KP == 0
                break
            end
        else
            V = V + invB2*(ΔQ./V);
            q = q+1;
            KP = 1;
        end
        iterations = iterations+1;
    end
    ϵ = maximum([activeerror(Ωb.P-Pcalc);reactiveerror(Ωb.Q-Qcalc)])
    return V,θ,Pcalc,Qcalc,ϵ,iterations
end
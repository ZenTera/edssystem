#Author: Lucas Zenichi Terada
#Institution: University of Campinas
#Description:

function switches()
    Ysw = zeros(ComplexF64,length(eachrow(Ωb)),length(eachrow(Ωb)))
    Κsw = []
    for bar in Ωb.ob
        Κaux = []
        for osw in eachrow(Ωsw)
            if osw.pos == true
                if osw.to == bar
                    push!(Κaux, osw.from)
                end
                if osw.from == bar
                    push!(Κaux,osw.to)
                end
            end
        end
        push!(Κsw,Κaux)
    end

    for osw in eachrow(Ωsw)
        k = osw.from;
        m = osw.to;
        if osw.pos == true
            Ysw[k,m] = -inv(osw.R + im*osw.X)/osw.a
            Ysw[m,k] = -inv(osw.R + im*osw.X)/osw.a
            Ysw[k,k] = Ysw[k,k] + (im*osw.bsh/2 + inv(osw.R + im*osw.X)/(osw.a^2))
            Ysw[m,m] = Ysw[m,m] + inv(osw.R + im*osw.X) + im*osw.bsh/2
        end
    end
    return Ysw, Κsw
end
#Κ = append!.(Κnet,Κsw)
#Y = Ynet + Ysw
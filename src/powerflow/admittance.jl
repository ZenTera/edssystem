#Author: Lucas Zenichi Terada
#Institution: University of Campinas
#Description:

function admittance()
    Ynet = zeros(ComplexF64,length(eachrow(Ωb)),length(eachrow(Ωb)))
    Κnet = []
    for bar in Ωb.ob
        Κaux = [bar]
        Ynet[bar,bar] = Ωb.bsh[bar]*im
        for ol in eachrow(Ωl)
            if ol.to == bar
                push!(Κaux, ol.from)
            end
            if ol.from == bar
                push!(Κaux,ol.to)
            end
        end
        push!(Κnet,Κaux)
    end

    for ol in eachrow(Ωl)
        k = ol.from;
        m = ol.to;
        Ynet[k,m] = -inv(ol.R + im*ol.X)/ol.a
        Ynet[m,k] = -inv(ol.R + im*ol.X)/ol.a
        Ynet[k,k] = Ynet[k,k] + (im*ol.bsh/2 + inv(ol.R + im*ol.X)/(ol.a^2))
        Ynet[m,m] = Ynet[m,m] + inv(ol.R + im*ol.X) + im*ol.bsh/2
    end
    return Ynet, Κnet
end

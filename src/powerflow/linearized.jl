#Author: Lucas Zenichi Terada
#Institution: University of Campinas

function linearized()
    global Ωb.P = (Ωb.PG - Ωb.PD)/Ωdata.Snom[1];
    size = length(eachrow(Ωb));
    P = zeros(Float64,size);
    Pp = zeros(Float64,size);
    θ = zeros(Float64,size);
    θ = setangle(θ);
    Bl = zeros(Float64,size,size);
    for ol in eachrow(Ωl)
        k = ol.from;
        m = ol.to;
        Bl[k,m] = -inv(ol.X);
        Bl[m,k] = -inv(ol.X);
        Bl[k,k] = Bl[k,k] + inv(ol.X);
        Bl[m,m] = Bl[m,m] + inv(ol.X);
    end
    if typeof(Ωsw) == DataFrame
        for osw in eachrow(Ωsw)
            k = osw.from;
            m = osw.to;
            Bl[k,m] = -inv(osw.X);
            Bl[m,k] = -inv(osw.X);
            Bl[k,k] = Bl[k,k] + inv(osw.X);
            Bl[m,m] = Bl[m,m] + inv(osw.X);
        end
    end
    for ob in eachrow(Ωb)
        if ob.Type == "SL"
            Bl[ob.ob,ob.ob] = Inf;
        end
    end
    θ = inv(Bl)*Ωb.P

    display(rad2deg.(θ))
    for ol in eachrow(Ωl)
        k = ol.from;
        m = ol.to;
        Pp[k] = Pp[k] - 0.5*real(inv(ol.R+im*ol.X))*(θ[k] - θ[m])^2;
        Pp[m] = Pp[m] - 0.5*real(inv(ol.R+im*ol.X))*(θ[m] - θ[k])^2;
    end
    display(Pp)
    θ = inv(Bl)*(Ωb.P + Pp)
    for ol in eachrow(Ωl)
        k = ol.from;
        m = ol.to;
        P[k] = P[k] + inv(ol.X)*(θ[k] - θ[m])
        P[m] = P[m] + inv(ol.X)*(θ[m] - θ[k])
    end
    P = round.(digits=2,(P - Pp)*Ωdata.Snom[1]);
    return θ,P
end
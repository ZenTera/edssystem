#Author: Lucas Zenichi Terada
#Institution: Lucas Zenichi Terada
#Description:
include("admittance.jl")

function error(ΔP,ΔQ)
    size = length(eachrow(Ωb))
    Pϵ = zeros(Float64,size)
    Qϵ = zeros(Float64,size)
    for ob in eachrow(Ωb)
        if ob.Type == "SL"
            Pϵ[ob.ob] = 0
            Qϵ[ob.ob] = 0
        elseif ob.Type == "PV"
            Pϵ[ob.ob] = abs(ΔP[ob.ob])
            Qϵ[ob.ob] = 0
        elseif ob.Type == "PQ"
            Pϵ[ob.ob] = abs(ΔP[ob.ob])
            Qϵ[ob.ob] = abs(ΔQ[ob.ob])
        end
    end
    ϵmax = maximum([Pϵ;Qϵ])
    return ϵmax
end

function setangle(θ)
    for ob in eachrow(Ωb)
        if ob.Type == "SL"
            θ[ob.ob] = deg2rad(Ωdata.ang[1]);
        end
    end
    return θ
end

function powerflow(Y,Κ)
    global Ωb.P = (Ωb.PG - Ωb.PD)/Ωdata.Snom[1]
    global Ωb.Q = (Ωb.QG - Ωb.QD)/Ωdata.Snom[1]
    size = length(eachrow(Ωb))
    G = real(Y);
    B = imag(Y);
    ϵ = Inf;
    #Create the Jacobian Submatrices
    size = length(eachrow(Ωb))
    θ = zeros(Float64,size)
    θ = setangle(θ);
    V = convert(Array,Ωb.Tb)
    Pcalc = zeros(Float64,size)
    Qcalc = zeros(Float64,size)
    H = zeros(Float64,size,size)
    N = zeros(Float64,size,size)
    M = zeros(Float64,size,size)
    L = zeros(Float64,size,size)

    iterations = 0;

    ############################################################################################
    for k in 1:size
        Pcalc[k] = 0;
        Qcalc[k] = 0;
        for m in Κ[k]
            θkm = θ[k] - θ[m];
            Pcalc[k] = Pcalc[k] + V[k]*V[m]*(G[k,m]*cos(θkm)+B[k,m]*sin(θkm));
            Qcalc[k] = Qcalc[k] + V[k]*V[m]*(G[k,m]*sin(θkm)-B[k,m]*cos(θkm));
        end
    end
    #############################################################################################

    while ϵ > 1e-4 && iterations < 20
        iterations = iterations+1;
        for k in 1:size
            if Ωb.Type[k] == "SL"
                H[k,k] = Inf
                L[k,k] = Inf
            elseif Ωb.Type[k] == "PV"
                L[k,k] = Inf
                H[k,k] = -B[k,k]*V[k]^2 - Qcalc[k]
            elseif Ωb.Type[k] == "PQ"
                H[k,k] = -B[k,k]*V[k]^2 - Qcalc[k]
                L[k,k] = -(Ωb.bq[k] + 2*Ωb.cq[k]*V[k])*Ωb.Q[k] + (Qcalc[k] - B[k,k]*V[k]^2)/V[k]        
            end
            N[k,k] = -(Ωb.bp[k] + 2*Ωb.cp[k]*V[k])*Ωb.P[k] + (Pcalc[k]+G[k,k]*V[k]^2)/V[k]
            M[k,k] = -G[k,k]*V[k]^2 + Pcalc[k]
        end
        for ol in eachrow(Ωl)
            k = ol.from;
            m = ol.to;
            θkm = θ[k] - θ[m]
            H[k,m] =  V[k]*V[m]*(G[k,m]*sin(θkm)-B[k,m]*cos(θkm))
            H[m,k] = -V[k]*V[m]*(G[k,m]*sin(θkm)+B[k,m]*cos(θkm))
            N[k,m] =  V[k]*(G[k,m]*cos(θkm) + B[k,m]*sin(θkm))
            N[m,k] =  V[m]*(G[k,m]*cos(θkm) - B[k,m]*sin(θkm))
            M[k,m] = -V[k]*V[m]*(G[k,m]*cos(θkm) + B[k,m]*sin(θkm))
            M[m,k] = -V[k]*V[m]*(G[k,m]*cos(θkm) - B[k,m]*sin(θkm))
            L[k,m] =  V[k]*(G[k,m]*sin(θkm)-B[k,m]*cos(θkm))
            L[m,k] = -V[m]*(G[k,m]*sin(θkm)+B[k,m]*cos(θkm))            
        end
        if typeof(Ωsw) == DataFrame
            for osw in eachrow(Ωsw)
                if osw.pos == 1
                    k = osw.from;
                    m = osw.to;
                    θkm = θ[k] - θ[m]
                    H[k,m] =  V[k]*V[m]*(G[k,m]*sin(θkm)-B[k,m]*cos(θkm))
                    H[m,k] = -V[k]*V[m]*(G[k,m]*sin(θkm)+B[k,m]*cos(θkm))
                    N[k,m] =  V[k]*(G[k,m]*cos(θkm) + B[k,m]*sin(θkm))
                    N[m,k] =  V[m]*(G[k,m]*cos(θkm) - B[k,m]*sin(θkm))
                    M[k,m] = -V[k]*V[m]*(G[k,m]*cos(θkm) + B[k,m]*sin(θkm))
                    M[m,k] = -V[k]*V[m]*(G[k,m]*cos(θkm) - B[k,m]*sin(θkm))
                    L[k,m] =  V[k]*(G[k,m]*sin(θkm)-B[k,m]*cos(θkm))
                    L[m,k] = -V[m]*(G[k,m]*sin(θkm)+B[k,m]*cos(θkm))
                end            
            end
        end

        J = [H N;M L];
        if det(J) == 0
            print("Singular\n")
            break
        end
        
        ΔP = (Ωb.ap + Ωb.bp.*V + Ωb.cp.*V.*V).*(Ωb.P) - Pcalc;
        ΔQ = (Ωb.aq + Ωb.bq.*V + Ωb.cq.*V.*V).*(Ωb.Q) - Qcalc;
        Δ = inv(J)*[ΔP;ΔQ];

        θ = θ + Δ[1:size];
        V = V + Δ[size+1:2*size];
        ############################################################################################
        for k in 1:size
            Pcalc[k] = 0;
            Qcalc[k] = 0;
            for m in Κ[k]
                θkm = θ[k] - θ[m];
                Pcalc[k] = Pcalc[k] + V[k]*V[m]*(G[k,m]*cos(θkm)+B[k,m]*sin(θkm));
                Qcalc[k] = Qcalc[k] + V[k]*V[m]*(G[k,m]*sin(θkm)-B[k,m]*cos(θkm));
            end
        end
        ##############################################################################################

        ΔP = (Ωb.ap + Ωb.bp.*V + Ωb.cp.*V.*V).*(Ωb.P) - Pcalc;
        ΔQ = (Ωb.aq + Ωb.bq.*V + Ωb.cq.*V.*V).*(Ωb.Q) - Qcalc;
        display(ΔP)
        display(ΔQ)
        print('\n')
        ϵ = error(ΔP,ΔQ);
    end
    return V,θ,Pcalc,Qcalc,ϵ,iterations
end
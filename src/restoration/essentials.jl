#Author: Lucas Zenichi Terada
#Institution: University of Campinas

function verify(df, bar, auxbus, auxnet, auxswt, radial, level, type)
    current_bar = 0
    if df.from == bar && df.level == -1
        current_bar = df.to
    elseif df.to == bar && df.level == -1
        current_bar = df.from
    end
    if current_bar > 0
        if type == "line"
            Ωl.level[df.num] = level
            push!(auxnet, df.num)
        elseif type =="switch"
            Ωsw.level[df.num] = level
            push!(auxswt, df.num)
        end
        if Ωb.level[current_bar] == -1
            Ωb.level[current_bar] = level
            push!(auxbus, current_bar)
        else
            radial = false
        end
    end
    return auxbus, auxnet, auxswt, radial
end

function radiality()
    (level, buslv, netlv, swtlv) = (1, [], [],[])
    (bussize, netsize, swtsize) = (length(eachrow(Ωb)),length(eachrow(Ωl)),length(eachrow(Ωsw)))
    (Ωb.level, Ωl.level, Ωsw.level) = (ones(Int64,bussize)*(-1),ones(Int64,netsize)*(-1),ones(Int64,swtsize)*(-1))
    #display(Ωb); display(Ωl); display(Ωsw)
    aux = []
    radial = true
    for ob in eachrow(Ωb)
        if ob.bustype == "feeder"
            Ωb.level[ob.num] = 1
            push!(aux,ob.num)
        end
    end
    push!(buslv,aux); push!(netlv,[]); push!(swtlv,[]);
    level = 2
    while radial == true
        (auxbus, auxnet, auxswt) = ([],[],[])
        for bar in buslv[level-1]
            for ol in eachrow(Ωl)
                (auxbus, auxnet, auxswt, radial) = verify(ol, bar, auxbus, auxnet, auxswt, radial, level, "line")
            end
            for osw in eachrow(Ωsw)
                if osw.pos == 1
                    (auxbus, auxswt, auxswt, radial) = verify(osw, bar, auxbus,auxnet, auxswt, radial, level, "switch")
                end
            end
        end
        if length(auxbus) > 0
            push!(buslv,auxbus); push!(netlv, auxnet); push!(swtlv,auxswt);
            level = level + 1
        else
            break
        end
    end
    return radial, level, buslv, netlv, swtlv
end

function update_network(level, buslv, netlv, swtlv)
    (bussize, netsize, swtsize) = (length(eachrow(Ωb)),length(eachrow(Ωl)),length(eachrow(Ωsw)))
    (Ωb.level, Ωl.level, Ωsw.level) = (ones(Int64,bussize)*(-1),ones(Int64,netsize)*(-1),ones(Int64,swtsize)*(-1))
    radial = true
    while radial == true
        (auxbus, auxnet, auxswt) = ([],[],[])
        for bar in buslv[level-1]
            for ol in eachrow(Ωl)
                (auxbus, auxnet, auxswt, radial) = verify(ol, bar, auxbus, auxnet, auxswt, radial, level, "line")
            end
            for osw in eachrow(Ωsw)
                if osw.pos == 1
                    (auxbus, auxswt, auxswt, radial) = verify(osw, bar, auxbus,auxnet, auxswt, radial, level, "switch")
                end
            end
        end
        if length(auxbus) > 0
            push!(buslv,auxbus); push!(netlv, auxnet); push!(swtlv,auxswt);
            level = level + 1
        else
            break
        end
    end
    return radial, level, buslv, netlv, swtlv
end


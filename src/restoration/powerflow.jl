#Author: Lucas Zenichi Terada
#Institution: University of Campinas

function error_calc(V, Ibus)
    diff = 0
    for bar in eachrow(Ωb)
        if bar.bustype == "load" && bar.level > 1
            aux = abs(bar.pd +bar.qd*im - V[bar.num]*conj(Ibus[bar.num]))
            diff = max(aux, diff)
        end
    end
    return diff
end

function identify(df, lv)
    if Ωb.level[df.from] == lv
        mul = -1+0im
        bar = df.from
    elseif Ωb.level[df.to] == lv
        mul = 1+0im
        bar = df.to
    else
        bar = 0
        mul = 0+0im
    end
    return bar, mul
end

function nodalcurrent(Ibus, df, lv)
    if Ωb.level[df.from] == lv
        return -Ibus[df.from]
    elseif Ωb.level[df.to] == lv
        return Ibus[df.to]
    end
end

function current(bar, Ikm, df, nplus)
    Inear = 0
    if bar == df.from
        Inear = Ikm[nplus]
    elseif bar == df.to
        Inear = -Ikm[nplus]
    end
    return Inear
end

function current_calc(lv, V, Ibus, Inet, Iswt, buslv, netlv, swtlv, level)
    for n in netlv[lv]
        Inet[n] = nodalcurrent(Ibus, eachrow(Ωl)[n], lv)
    end
    for s in swtlv[lv]
        Iswt[s] = nodalcurrent(Ibus, eachrow(Ωsw)[s], lv)
    end
    if lv+1 < level
        for n in netlv[lv]
            (bar, mul) = identify(eachrow(Ωl)[n], lv)
            for nplus in netlv[lv+1]
                Inet[n] = Inet[n] + mul*current(bar, Inet, eachrow(Ωl)[nplus], nplus)
            end
            for splus in swtlv[lv+1]
                Inet[n] = Inet[n] + mul*current(bar, Iswt, eachrow(Ωsw)[splus], splus)
            end
        end
        for s in swtlv[lv]
            (bar, mul) = identify(eachrow(Ωsw)[s], lv)
            for splus in swtlv[lv+1]
                Iswt[s] = Iswt[s] + mul*current(bar, Iswt, eachrow(Ωsw)[splus], splus)
            end
            for nplus in netlv[lv+1]
                Iswt[s] = Iswt[s] + mul*current(bar, Inet, eachrow(Ωl)[nplus], nplus)
            end
        end

    end
    return Inet, Iswt
end

function voltage_calc(lv, V, Ibus, Inet, Iswt, buslv, netlv, swtlv, level)
    for n in netlv[lv]
        if Ωb.level[Ωl.from[n]] > Ωb.level[Ωl.to[n]]
            V[Ωl.from[n]] = V[Ωl.to[n]] - Inet[n]*(Ωl.r[n]+im*Ωl.x[n])
        else
          V[Ωl.to[n]] = V[Ωl.from[n]] - Inet[n]*(Ωl.r[n]+im*Ωl.x[n])
        end
    end
    for s in swtlv[lv]
        if Ωb.level[Ωsw.from[s]] > Ωb.level[Ωsw.to[s]]
            V[Ωsw.from[s]] = V[Ωsw.to[s]] - Iswt[s]*(Ωsw.r[s]+im*Ωsw.x[s])
        else
            V[Ωsw.to[s]] = V[Ωsw.from[s]] - Iswt[s]*(Ωsw.r[s]+im*Ωsw.x[s])
        end
    end
    return V
end


function initial_voltage(buslv, netlv, swtlv, level)
    V = zeros(ComplexF64, length(eachrow(Ωb)))
    for bar in buslv[1]
        V[bar] = Ωb.baseKV[bar]/sqrt(3)
    end
    for lv in 2:level-1
        for line in netlv[lv]
            if Ωb.level[Ωl.from[line]] < Ωb.level[Ωl.to[line]]
                V[Ωl.to[line]] = V[Ωl.from[line]]
            elseif Ωb.level[Ωl.to[line]] < Ωb.level[Ωl.from[line]]
                V[Ωl.from[line]] = V[Ωl.to[line]]
            end
        end
        for switch in swtlv[lv]
            if Ωb.level[Ωsw.from[switch]] < Ωb.level[Ωsw.to[switch]]
                V[Ωsw.to[switch]] = V[Ωsw.from[switch]]
            elseif Ωb.level[Ωsw.to[switch]] < Ωb.level[Ωsw.from[switch]]
                V[Ωsw.from[switch]] = V[Ωsw.to[switch]]
            end
        end
    end
    return V
end

function sweeppowerflow(buslv, netlv, swtlv, level)
    Ibus = zeros(ComplexF64,length(eachrow(Ωb)))
    Inet = zeros(ComplexF64,length(eachrow(Ωl)))
    Iswt = zeros(ComplexF64,length(eachrow(Ωsw)))
    V = initial_voltage(buslv, netlv, swtlv, level)
    diff = Inf
    iteration = 0
    while diff > 1e-5 && iteration < 15
        # Passo 2: Injeção de corrente
        for bar in eachrow(Ωb)
            if V[bar.num] != 0
                Ibus[bar.num] = conj((bar.pd+im*bar.qd)/(snom*V[bar.num]))
            end
        end
        # Passo 3: (Backward sweep): Fluxo de corrente para cada level
        for lv in level-1:-1:1
            (Inet, Iswt) = current_calc(lv, V, Ibus, Inet, Iswt, buslv, netlv, swtlv, level)
        end
        # Passo 4: (Forward sweep:) Tensões nas barras
        for lv in 1:level-1
            V = voltage_calc(lv, V, Ibus, Inet, Iswt, buslv, netlv, swtlv, level)
        end
        diff = error_calc(V, Ibus)
        iteration = iteration + 1
    end
  return V, Ibus, Inet, Iswt, diff, iteration
end

function states(V, Inet, Iswt)
    Ωl.Ikm = round.(abs.(Inet), digits=4)
    Ωsw.Ikm = round.(abs.(Iswt), digits=4)
    Ωb.Vp = round.(abs.(V),digits=4)
    Ωb.Vl = round.(abs.(V*sqrt(3)),digits=4)
    S = zeros(ComplexF64,length(eachrow(Ωl)))
    for line in eachrow(Ωl)
        S[line.num] = V[line.from]*conj(Inet[line.num])
    end
    Ωl.Pkm = round.(real(S),digits=4)
    Ωl.Qkm = round.(imag(S),digits=4)
    S = zeros(ComplexF64,length(eachrow(Ωsw)))
    for line in eachrow(Ωsw)
        S[line.num] = V[line.from]*conj(Iswt[line.num])
    end
    Ωsw.Pkm = round.(real(S),digits=4)
    Ωsw.Qkm = round.(imag(S),digits=4)
end

function loss()
    Ploss = 0.0
    for line in eachrow(Ωl)
        Ploss = Ploss + line.r*line.Ikm^2
    end
    for switch in eachrow(Ωsw)
        Ploss = Ploss + switch.r*switch.Ikm^2
    end
    return Ploss
end
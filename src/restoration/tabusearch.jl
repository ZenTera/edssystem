#Author: Lucas Zenichi Terada
#Institution: University of Campinas

function tabusearch(BTmax, upper, lower)
    sol = zeros(Int64, length(eachrow(Ωsw)))
    Ωsw.pos = sol
    (radial, level, buslv, netlv, swtlv) = radiality()
    (V, Ibus, Inet, Iswt, diff, iteration) = sweeppowerflow(buslv, netlv, swtlv, level)
    states(V, Inet, Iswt)
    best_cost = cost(cr, cch, cls)
    (iter, bestiter, tabulist) = (0, 0, [])
    (best_sol, config) = (sol, copy(netlv))
    (reset_sol, reset_cost) = (copy(best_sol), best_cost)
    best_config = copy(config)
    while iter - bestiter < BTmax
        iter = iter + 1
        print("MELHOR ITERACAO: ",bestiter,'\n')
        print("ITERACAO: ", iter,"\n\n\n")
        for i in 1:length(eachrow(Ωsw))
            neighbor = copy(sol)
            neighbor[i] = neighbor[i] ⊻ 1
            Ωsw.pos = neighbor
            print("\n\nLISTA TABU: ", tabulist,"\n") 
            int_value = bin2int()
            print("SOLUÇÃO TESTADA: ",int_value, "\n")
            print(Ωsw.pos,"\n")
            if !(int_value in tabulist)
                (radial, level, buslv, netlv, swtlv) = radiality()
                if (netlv == config)
                    print("Entrou na msm topologia\n")
                    push!(tabulist, int_value) 
                    continue
                elseif radial
                    if !(isolated())
                        print("Não isolado\n")
                        push!(tabulist, int_value)
                        continue
                    end
                    (V, Ibus, Inet, Iswt, diff, iteration) = sweeppowerflow(buslv, netlv, swtlv, level)
                    states(V, Inet, Iswt)
                    if !voltage_constraint(upper, lower)
                        push!(tabulist, int_value)
                        print("Não respeita tensão\n")
                        continue
                    elseif !current_constraint()
                        push!(tabulist, int_value)
                        print("Não respeita corrente\n")
                        continue
                    else
                        current_cost = cost(cr, cch, cls)
                        if current_cost < best_cost
                            (best_sol, best_cost, bestiter) = (copy(Ωsw.pos), current_cost, iter)
                            print("\n\n\n ======SOLUCAO ENCONTRADA:======= \n", sol)
                            print("\nMELHOR ITERACAO: ", bestiter,'\n')
                            print("\nCUSTO: ", best_cost, "\n\n\n\n")
                            best_config = copy(netlv)
                        else
                            print("Não é a melhor solução\n")
                        end
                    end
                end
            else
                print("MOVIMENTO TABU\n")
            end
        end
        Ωsw.pos = best_sol
        push!(tabulist, bin2int())
        if iter > bestiter
            if reset_cost > best_cost
                (reset_sol, reset_cost) = (copy(best_sol), best_cost)
                best_cost = Inf
                best_sol = zeros(Int64, length(eachrow(Ωsw)))
            end
        end
        config = best_config
        sol = copy(best_sol)
    end
    best_cost = reset_cost
    Ωsw.pos = reset_sol
    print(sol)
    (radial, level, buslv, netlv, swtlv) = radiality()
    (V, Ibus, Inet, Iswt, diff, iteration) = sweeppowerflow(buslv, netlv, swtlv, level)
    states(V, Inet, Iswt)
    return iter, best_cost, length(tabulist)
end
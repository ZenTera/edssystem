# Author: Lucas Zenichi Terada
# Institution: University of Campinas

function voltage_constraint(upper, lower)
    respect = true
    for bar in eachrow(Ωb)
        if bar.Vl/bar.baseKV > upper && bar.level > -1
            respect = false
            break
        end
        if bar.Vl/bar.baseKV < lower && bar.level > -1
            respect = false
            break
        end
    end
    return respect
end

function current_constraint()
    respect = true
    for line in eachrow(Ωl)
        if abs(line.Ikm) > line.Imax
            respect = false
            break
        end
    end
    for switch in eachrow(Ωsw)
        if abs(switch.Ikm) > switch.Imax
            respect = false
            break
        end
    end
    return respect
end

function isolated()
    respect = true
    for bar in eachrow(Ωb)
        if zone_status[bar.zb+1] == 0 && bar.level > -1
            respect = false
            break
        end
    end
    return respect
end

function bin2int()
    size = length(eachrow(Ωsw))
    value = 0
    for i in size-1:-1:0
        value = value + Ωsw.pos[size-i]*2^i
    end
    return value
end

function cost(cr, cch, cls)
    price = 0.0
    for bar in eachrow(Ωb)
        if bar.level == -1
            price = price + cr*bar.pd
        end
    end
    nchanges = sum(abs.(Ωsw.ini - Ωsw.pos))
    price = price + cch * nchanges
    price = price + cls * loss()
    return price
end
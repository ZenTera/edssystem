# IC 2020

## Instalation

Install Julia console on your computer and clone this repository to any folder on your system then install the following packages:

*  Gtk.jl
*  CSV.jl
*  DataFrames.jl
*  LinearAlgebra.jl

The packages can be installed using the command Pkg.add("**Package name**") when loaded the Pkg.jl package through the command **using Pkg**.


## Usage

To use the program run the command **julia -e main.jl** in the program directory. 
For the following cases of power distribution, perform the following procedures:

1.  **Power Flow:** Load the bus file and branches file all in csv format;
2.  **Reconfiguration:** Load the bus, branches and switches file all in csv format;
3.  **Restoration:** Load the bus, branches and switches file all in csv format;
4.  **Expansion:** *ongoing procedure*

For all procedures, it is necessary to load the file with the nominal data of the network.